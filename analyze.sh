#!/usr/bin/env bash
#
WDIR=results/
TASKNO=$1
#
pushd ${WDIR}${TASKNO}
#
mkdir n nbar p pbar p_prim
mv *_n.dat n/; mv *_nbar.dat nbar/; mv *_pbar.dat pbar/; mv *_p.dat p/;mv *_p_prim.dat p_prim/
#
cd n/
ls -1 > list
cat ../../header.sec > n.dat
find . -maxdepth 1 -type f -name 'tmp_*.dat' -print0 | sort -z | xargs -0 cat -- >> n.dat
rm tmp_*.dat
#
cd ../p/
ls -1 > list
cat ../../header.sec > p.dat
find . -maxdepth 1 -type f -name 'tmp_*.dat' -print0 | sort -z | xargs -0 cat -- >> p.dat
rm tmp_*.dat
#
cd ../pbar/
ls -1 > list
cat ../../header.sec > pbar.dat
find . -maxdepth 1 -type f -name 'tmp_*.dat' -print0 | sort -z | xargs -0 cat -- >> pbar.dat
rm tmp_*.dat
#
cd ../nbar/
ls -1 > list
cat ../../header.sec > nbar.dat
find . -maxdepth 1 -type f -name 'tmp_*.dat' -print0 | sort -z | xargs -0 cat -- >> nbar.dat
rm tmp_*.dat
#
cd ../p_prim/
ls -1 > list
cat ../../header.prim > p_prim.dat
find . -maxdepth 1 -type f -name 'tmp_*.dat' -print0 | sort -z | xargs -0 cat -- >> p_prim.dat
rm tmp_*.dat
#
popd
