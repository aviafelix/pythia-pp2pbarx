#!/bin/sh
cd libpythia8/ \
&& ./configure \
	--enable-64bit \
	--enable-shared \
&& make -j$(nproc) \
&& cd ..
# --prefix= \
# --prefix-bin= \
# --prefix-lib= \
# --prefix-include= \
# --prefix-share= \
