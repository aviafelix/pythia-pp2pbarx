#!/usr/bin/env bash
echo " *** begin ***"
date
echo " -- update submodule --"
git submodule init
git submodule update
echo " -- Installing boost..."
#sudo apt-get install -f
#sudo dpkg --configure -a
#sudo apt-get clean
#sudo apt-get update
sudo apt-get autoremove
sudo apt-get install --yes --force-yes  libboost-all-dev
echo " -- Ok"
echo " -- configure and make"
./configure && make -j$(nproc)
echo "-- ok"
date
echo " -- go to examples/ dir"
# pushd examples/
cd examples/
echo "%%%%%%%%%%%%%%%%%%%%%%"
ls -la
echo "%%%%%%%%%%%%%%%%%%%%%%"
mkdir results
mkdir results/logs/
echo " -- make inside it"
echo " -- path: `pwd`"
date
make -j$(nproc) 2>&1
echo " -- ok"
echo "%%%%%%%%%%%%%%%%%%%%%%"
ls -la
echo "%%%%%%%%%%%%%%%%%%%%%%"
date
# popd
cd ../
echo " *** end ***"
