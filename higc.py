#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt
# import matplotlib
from matplotlib import rc
import argparse
import numpy as np
import math

font = {
    'family': 'Verdana',
    'weight': 'normal',
}

agg = {
    'path.chunksize': 100000,
}

figure = {
    'dpi': 100,
    # 'figsize': [19.2, 10.8],
    # 'subplot.bottom': 0.06,
    # 'subplot.hspace': 0.2,
    # 'subplot.left': 0.06,
    # 'subplot.right': 0.96,
    # 'subplot.top': 0.94,
    # 'subplot.wspace': 0.2,
}

savefig = {
    'dpi': 100,
}

rc('font', **font)
rc('figure', **figure)
rc('savefig', **savefig)
rc('agg', **agg)

if __name__ == '__main__':

    # df = pd.read_csv(StringIO(csv),
    #     header=0,
    #     index_col=["date", "loc"], 
    #     usecols=["date", "loc", "x"],
    #     parse_dates=["date"])

    print("Ok!")
