#!/usr/bin/env bash

./higc \
    --model WH03 \
    --window-width 0.5 \
    --n-points 15 \
    --logscale-mean \
    --logscale-dots \
    --primary-protons=./results/001/p_prim/WH03_lin_p_prim.dat \
    --secondary-protons=./results/001/p/WH03_lin_p.dat \
    --secondary-antiprotons=./results/001/pbar/WH03_lin_pbar.dat \
    --secondary-neutrons=./results/001/n/WH03_lin_n.dat \
    --secondary-antineutrons=./results/001/nbar/WH03_lin_nbar.dat \
    --output-file=./results/001/test_
    # --output-file=./results/001/test_ \
    # --relative-window-width
