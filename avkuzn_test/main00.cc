#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "Pythia8/Pythia.h"

using namespace Pythia8;

typedef struct {
    std::vector<double> x;
    std::vector<double> y;
} histogram;

/**********************************************************************************
* Calculates histogram and returns pairs of doubles (E_mean, N_count)
*/
histogram histogram_means(std::vector<double> data, unsigned int binsCount, double norm) {

    histogram hist;

    if ((data.size() == 0) || (binsCount == 0)) {
        // Empty vector
        return hist;
    }

    double minValue = *std::min_element(data.begin(), data.end());
    double maxValue = *std::max_element(data.begin(), data.end());

    double deltaE = (maxValue - minValue) / binsCount;

    // Loop over bins
    for (unsigned int i = 0; i < binsCount; ++i) {

        unsigned int nCount = 0;
        double eSumm = 0.0;

        double leftBound = minValue + static_cast<double>(i) * deltaE;
        // double rightBound = leftBound + deltaE;
        double rightBound = minValue + static_cast<double>(i + 1) * deltaE;

        // Loop over all elements of data vector
        for (unsigned int k = 0, k_end = data.size(); k < k_end; ++k) {

            if ( (data[k] >= leftBound) && (data[k] <= rightBound) ) {

                nCount += 1;
                eSumm += data[k];
            }

        }

        if (nCount == 0) {

            eSumm = leftBound + 0.5 * deltaE;

        } else {

            eSumm /= nCount;
        }

        hist.x.push_back(eSumm);
        hist.y.push_back(static_cast<double>(nCount) / (norm * deltaE));

    }

    // std::cout
    //     << "`data` size: "
    //     << data.size()
    //     << std::endl
    //     << "`minValue`: "
    //     << minValue
    //     << std::endl
    //     << "`maxValue`: "
    //     << maxValue
    //     << std::endl;

    return hist;
}
// **********************************************************************************

/**********************************************************************************
* Calculates log scale (abscissa) histogram and returns pairs of doubles (E_mean, N_count)
*/
histogram histogram_means_log(std::vector<double> data, unsigned int binsCount, double norm) {

    histogram hist;

    if ((data.size() == 0) || (binsCount == 0)) {
        // Empty vector
        return hist;
    }

    double minValue = *std::min_element(data.begin(), data.end());
    double maxValue = *std::max_element(data.begin(), data.end());

    double maxDivMin = maxValue / minValue;

    double logMinValue = std::log(minValue);
    double logMaxValue = std::log(maxValue);

    double intervalSize = logMaxValue - logMinValue;

    double deltaXi = (logMaxValue - logMinValue) / binsCount;

    // Loop over bins
    for (unsigned int i = 0; i < binsCount; ++i) {

        unsigned int nCount = 0;

        double eSumm = 0.0;
        double xiSumm = 0.0;

        // double leftBound = logMinValue + static_cast<double>(i) * deltaXi;
        double leftBound = logMinValue + intervalSize * (static_cast<double>(i) / binsCount);
        // double rightBound = leftBound + deltaXi;
        // double rightBound = logMinValue + static_cast<double>(i + 1) * deltaXi;
        double rightBound = logMaxValue - intervalSize * (1.0 - static_cast<double>(i + 1) / binsCount);

        // Loop over all elements of data vector
        for (unsigned int k = 0, k_end = data.size(); k < k_end; ++k) {

            double logData = std::log(data[k]);

            if ( (logData >= leftBound) && (logData <= rightBound) ) {

                nCount += 1;
                eSumm += data[k];
                xiSumm += logData;
            }

        }

        if (nCount == 0) {

            eSumm = std::pow(minValue, 1.0 - (static_cast<double>(i) + 0.5) / binsCount)
                  * std::pow(maxValue, (static_cast<double>(i) + 0.5) / binsCount),
            xiSumm = leftBound + 0.5 * deltaXi;

        } else {

            eSumm /= nCount;
            xiSumm /= nCount;
        }

        // double deltaE = std::exp(rightBound) - std::exp(leftBound);
        double deltaE = minValue * std::pow(maxDivMin, static_cast<double>(i) / binsCount) * (std::pow(maxDivMin, 1. / binsCount) - 1.0);

        hist.x.push_back(eSumm);
        hist.y.push_back(static_cast<double>(nCount) / (norm * deltaE));

        // hist.x.push_back(std::exp(xiSumm));
        // hist.y.push_back(static_cast<double>(nCount) / (norm * deltaE));

        // hist.x.push_back(
        //     std::pow(minValue, 1.0 - (static_cast<double>(i) + 0.5) / binsCount)
        //   * std::pow(maxValue, (static_cast<double>(i) + 0.5) / binsCount)
        // );
        // hist.y.push_back(static_cast<double>(nCount) / (norm * deltaE));

    }

    return hist;
}
// **********************************************************************************

/**********************************************************************************
* Calculates histogram and returns pairs of doubles (E_mean, N_count)
*/
histogram histogram_discrete(std::vector<double> data, unsigned int binsCount, double norm) {

    histogram hist = {
        std::vector<double> (binsCount, 0.0),
        std::vector<double> (binsCount, 0.0)
        // .x = std::vector<double> (binsCount, 0.0),
        // .y = std::vector<double> (binsCount, 0.0)
    };

    if ((data.size() == 0) || (binsCount == 0)) {
        // Empty vector
        return hist;
    }

    double minValue = *std::min_element(data.begin(), data.end());
    double maxValue = *std::max_element(data.begin(), data.end());

    // Loop over all elements of data vector
    for (unsigned int k = 0, k_end = data.size(); k < k_end; ++k) {

        unsigned int idx = static_cast<unsigned int>(std::floor(static_cast<double>(binsCount) * (data[k] - minValue) / (maxValue - minValue)));

        // this is strict equality in fact
        if (idx >= binsCount) {

            // idx = binsCount -1;
            idx -= 1;
        }

        hist.x[ idx ] += data[k];
        hist.y[ idx ] += 1.0;
    }

    double deltaE = (maxValue - minValue) / binsCount;

    // Loop over bins
    for (unsigned int i = 0; i < binsCount; ++i) {
        // Mean value on abscissa
        hist.x[i] /= hist.y[i];
        // Normalize by the norm
        hist.y[i] /= (norm * deltaE);
        // hist.y[i] /= norm;
    }

    return hist;
}
// **********************************************************************************

/**********************************************************************************
* Calculates histogram and returns pairs of doubles in log scale (E_mean, N_count)
*/
histogram histogram_discrete_log(std::vector<double> data, unsigned int binsCount, double norm) {

    histogram hist = {
        std::vector<double> (binsCount, 0.0),
        std::vector<double> (binsCount, 0.0)
        // .x = std::vector<double> (binsCount, 0.0),
        // .y = std::vector<double> (binsCount, 0.0)
    };

    if ((data.size() == 0) || (binsCount == 0)) {
        // Empty vector
        return hist;
    }

    double minValue = *std::min_element(data.begin(), data.end());
    double maxValue = *std::max_element(data.begin(), data.end());

    double maxDivMin = maxValue / minValue;

    double logMinValue = std::log(minValue);
    double logMaxValue = std::log(maxValue);

    // Loop over all elements of data vector
    for (unsigned int k = 0, k_end = data.size(); k < k_end; ++k) {

        // unsigned int idx = static_cast<unsigned int>(std::floor(static_cast<double>(binsCount - 1) * (std::log(data[k]) - logMinValue) / (logMaxValue - logMinValue) + 0.5));
        unsigned int idx = static_cast<unsigned int>(std::floor(static_cast<double>(binsCount) * (std::log(data[k]) - logMinValue) / (logMaxValue - logMinValue)));

        // this is strict equality in fact
        if (idx >= binsCount) {

            // idx = binsCount -1;
            idx -= 1;
        }

        // *** this is for arithmetic mean --- correct way!
        hist.x[ idx ] += data[k];

        // *** or this is for geometric mean
        // hist[ idx ].x += std::log(data[k]);

        hist.y[ idx ] += 1.0;
    }

    // Loop over bins
    for (unsigned int i = 0; i < binsCount; ++i) {

        double deltaE = minValue * std::pow(maxDivMin, static_cast<double>(i) / binsCount) * (std::pow(maxDivMin, 1. / binsCount) - 1.0);

        // *** this is for arithmetic mean --- correct way!
        hist.x[i] = hist.x[i] / hist.y[i];

        // *** or this is for geometric mean
        // hist.x[i] = std::exp(hist.x[i] / hist.y[i]);

        hist.y[i] /= (norm * deltaE);
        // hist.y[i] /= norm;
    }

    return hist;
}
// **********************************************************************************

/**********************************************************************************
* Calculates histogram and returns pairs of doubles (E_mean, N_count)
*/
histogram histogram_window(std::vector<double> data,
                                                      unsigned int dotsCount, double norm,
                                                      double windowSize, bool isRelativeWindow) {

    histogram hist;

    if ((data.size() == 0) || (dotsCount == 0)) {
        // Empty vector
        return hist;
    }

    double minValue = *std::min_element(data.begin(), data.end());
    double maxValue = *std::max_element(data.begin(), data.end());

    double _windowSize = isRelativeWindow ? windowSize * (maxValue - minValue) : windowSize;
    // sizes of energy intervals for dotsCount dots
    double deltaE = (maxValue - minValue) / dotsCount;

    for (unsigned int i = 0; i <= dotsCount; ++i) {

        unsigned int nCount = 0;
        double eSumm = 0.0;

        // double h = static_cast<double>(i) / dotsCount;
        // leftBound = (1 - h) * minValue + h * maxValue - 0.5 * _windowSize;
        double leftBound = minValue + static_cast<double>(i) * deltaE - 0.5 * _windowSize;

        // double h = static_cast<double>(i + 1) / dotsCount;
        // rightBound = (1 - h) * minValue + h * maxValue + 0.5 * _windowSize;
        double rightBound = minValue + static_cast<double>(i) * deltaE + 0.5 * _windowSize;

        // correct bounds
        if (leftBound < minValue) {

            leftBound = minValue;
        }

        if (rightBound > maxValue) {

            rightBound = maxValue;
        }

        // find normalization window size
        double normWindowSize = rightBound - leftBound;

        // Loop over all elements of data vector
        for (unsigned int k = 0, k_end = data.size(); k < k_end; ++k) {

            if ( (data[k] >= leftBound) && (data[k] <= rightBound) ) {

                nCount += 1;
                eSumm += data[k];
            }

        }

        if (nCount == 0) {

            eSumm = leftBound + 0.5 * deltaE;

        } else {

            eSumm /= nCount;
        }

        hist.x.push_back(eSumm);
        hist.y.push_back(static_cast<double>(nCount) / (norm * normWindowSize));

    }

    return hist;
}
// **********************************************************************************

/**********************************************************************************
* Calculates histogram and returns pairs of doubles (E_mean, N_count)
*/
histogram histogram_window_log(std::vector<double> data, unsigned int dotsCount, double norm, double relativeWindowSize) {

    histogram hist;

    if ((data.size() == 0) || (dotsCount == 0)) {
        // Empty vector
        return hist;
    }

    double minValue = *std::min_element(data.begin(), data.end());
    double maxValue = *std::max_element(data.begin(), data.end());

    double logMinValue = std::log(minValue);
    double logMaxValue = std::log(maxValue);

    // double _windowSize = isRelativeWindow ? relativewindowSize * (logMaxValue - logMinValue) : relativewindowSize;
    double _windowSize = relativeWindowSize * (logMaxValue - logMinValue);
    // sizes of energy intervals for dotsCount dots
    double deltaXi = (logMaxValue - logMinValue) / dotsCount;

    for (unsigned int i = 0; i <= dotsCount; ++i) {

        unsigned int nCount = 0;

        double eSumm = 0.0;
        double xiSumm = 0.0;

        // double h = static_cast<double>(i) / dotsCount;
        // leftBound = (1 - h) * logMinValue + h * logMaxValue - 0.5 * _windowSize;
        double leftBound = logMinValue + static_cast<double>(i) * deltaXi - 0.5 * _windowSize;

        // double h = static_cast<double>(i + 1) / dotsCount;
        // rightBound = (1 - h) * logMinValue + h * logMaxValue + 0.5 * _windowSize;
        double rightBound = logMinValue + static_cast<double>(i) * deltaXi + 0.5 * _windowSize;

        // correct bounds
        if (leftBound < logMinValue) {

            leftBound = logMinValue;
        }

        if (rightBound > logMaxValue) {

            rightBound = logMaxValue;
        }

        // find normalization window size
        double normWindowSize = std::exp(rightBound) - std::exp(leftBound);

        // Loop over all elements of data vector
        for (unsigned int k = 0, k_end = data.size(); k < k_end; ++k) {

            double logData = std::log(data[k]);

            if ( (logData >= leftBound) && (logData <= rightBound) ) {

                nCount += 1;
                eSumm += data[k];
                xiSumm += logData;
            }

        }

        if (nCount == 0) {

            eSumm = std::pow(minValue, 1.0 - static_cast<double>(i) / dotsCount)
                  * std::pow(maxValue, static_cast<double>(i) / dotsCount),
            xiSumm = leftBound + 0.5 * deltaXi;

        } else {

            eSumm /= nCount;
            xiSumm /= nCount;
        }

        hist.x.push_back(eSumm);
        hist.y.push_back(static_cast<double>(nCount) / (norm * normWindowSize));

    }

    return hist;
}
// **********************************************************************************

/**********************************************************************************
* Saves histogram data to specified file
*/
void saveHistogram(histogram data, std::string outputFile) {

    if (outputFile == "") {
        return;
    }

    ofstream oFile_x(("x-" + outputFile).c_str(), ios::out | ios::binary);
    ofstream oFile_y(("y-" + outputFile).c_str(), ios::out | ios::binary);

    if (oFile_x.fail() || oFile_y.fail()) {
        return;
    }

    oFile_x.write((char*)&data.x[0], sizeof(double)*data.x.size());
    oFile_y.write((char*)&data.y[0], sizeof(double)*data.y.size());

    oFile_y.close();
    oFile_x.close();
}
// **********************************************************************************

int main() {
    
    // Generator
    Pythia pythia;

    // Read commands from external file
    pythia.readFile("main00.cmnd");

    Event& event  = pythia.event;

    // Extract settings to be used in the main program.
    int nEvent = pythia.mode("Main:numberOfEvents");
    int nAbort = pythia.mode("Main:timesAllowErrors");

    // Initialize.
    pythia.init();

    // Begin event loop.
    int iAbort = 0;

    std::vector<double> energyValues_pbar;
    std::vector<double> energyValues_nbar;
    double beamAEnergy = 1000.0;

    for (int iEvent = 0; iEvent < nEvent; ++iEvent) {

        beamAEnergy += 0.25;
        pythia.settings.parm("Beams:eA", beamAEnergy);
        cout << "Energy changed to:" << beamAEnergy << "\n";
        cout << " :: : " << pythia.settings.parm("Beams:eA") << "\n";

        // Generate events. Quit if many failures.
        if (!pythia.next()) {
            if (++iAbort < nAbort) continue;
            cout << " Event generation aborted prematurely, owing to error!\n";
            break;
        }

        int eventSize = event.size();

        for (int i = 0; i < eventSize; ++i) {
            if (event[i].isFinal()) {
                switch (event[i].id()) {
                    // antiprotons
                    case -2212:
                    
                        energyValues_pbar.push_back(event[i].e());

                        break;

                    // antineutrons:
                    case -2112:

                        energyValues_nbar.push_back(event[i].e());

                        break;

                    default:
                        break;
                }
            }
        }
    }

    saveHistogram(
        histogram_means(energyValues_pbar, 20, static_cast<double>(energyValues_pbar.size())),
        "histogram_means_pbar.dat"
    );

    saveHistogram(
        histogram_means(energyValues_nbar, 20, static_cast<double>(energyValues_nbar.size())),
        "histogram_means_nbar.dat"
    );

    saveHistogram(
        histogram_means_log(energyValues_pbar, 10, static_cast<double>(energyValues_pbar.size())),
        "histogram_means_log_pbar.dat"
    );

    saveHistogram(
        histogram_means_log(energyValues_nbar, 10, static_cast<double>(energyValues_nbar.size())),
        "histogram_means_log_nbar.dat"
    );

    saveHistogram(
        histogram_window(energyValues_pbar, 20, static_cast<double>(energyValues_pbar.size()), 0.1, true),
        "histogram_window_pbar.dat"
    );

    saveHistogram(
        histogram_window(energyValues_nbar, 20, static_cast<double>(energyValues_nbar.size()), 0.1, true),
        "histogram_window_nbar.dat"
    );

    saveHistogram(
        histogram_window_log(energyValues_pbar, 20, static_cast<double>(energyValues_pbar.size()), 0.1),
        "histogram_window_log_pbar.dat"
    );

    saveHistogram(
        histogram_window_log(energyValues_nbar, 20, static_cast<double>(energyValues_nbar.size()), 0.1),
        "histogram_window_log_nbar.dat"
    );

    // save generated data to binary files
    ofstream oFile_pbar("pbar.bin", ios::out | ios::binary);
    ofstream oFile_nbar("nbar.bin", ios::out | ios::binary);

    if (oFile_pbar.fail() || oFile_nbar.fail()) {
        return -1;
    }

    oFile_pbar.write((char*)&energyValues_pbar[0], sizeof(double)*energyValues_pbar.size());
    oFile_nbar.write((char*)&energyValues_nbar[0], sizeof(double)*energyValues_nbar.size());

    oFile_pbar.close();
    oFile_nbar.close();

    std::cout
        << "\nPrimary protons number to secondary antiprotons number ratio: "
        << static_cast<double>(nEvent) / energyValues_pbar.size()
        << "\nPrimary protons number to secondary antineutrons number ratio: "
        << static_cast<double>(nEvent) / energyValues_nbar.size()
        << std::endl;
}
