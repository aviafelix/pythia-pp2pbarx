// **************************************
// ** Testing for reinitialization time
// ** and example of how not to print the banner
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "Pythia8/Pythia.h"

using namespace Pythia8;

typedef struct {
    std::vector<double> x;
    std::vector<double> y;
} histogram;

int main() {
    
    // Generator
    Pythia pythia("../share/Pythia8/xmldoc", false);

    // Read commands from external file
    pythia.readFile("main03.cmnd");

    Event& event  = pythia.event;

    // Extract settings to be used in the main program.
    int nEvents = pythia.mode("Main:numberOfEvents");
    int nAbort = pythia.mode("Main:timesAllowErrors");

    // Initialize.
    pythia.init();

    // Begin event loop.


    int iAbort = 0;

    for (int iEvent = 0; iEvent < nEvents; ++iEvent) {
    
        // Generate events. Quit if many failures.
        if (!pythia.next()) {
            if (++iAbort < nAbort) continue;
            cout << " Event generation aborted prematurely, owing to error!\n";
            break;
        }

        int eventSize = event.size();

        std::cout <<  " === Event #" << iEvent << std::endl;

        for (int i = 0; i < eventSize; ++i) {
            if (event[i].isFinal()) {
                std::cout
                    << "id = " << event[i].id()
                    << " (" << event[i].name() << ")"
                    << " with E = " << event[i].e() << " GeV"
                    << std::endl;
            }
        }
    }

}
