#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include "Pythia8/Pythia.h"
#ifdef WITHGPERFTOOLS
#include <gperftools/profiler.h>
#endif

using namespace Pythia8;

typedef struct {
    std::vector<double> x;
    std::vector<double> y;
} histogram;

int main() {
    
    // Generator
    Pythia pythia("../share/Pythia8/xmldoc", false);

    // Read commands from external file
    pythia.readFile("main01.cmnd");

    Event& event  = pythia.event;

    // Extract settings to be used in the main program.
    int nEvents = pythia.mode("Main:numberOfEvents");
    int nAbort = pythia.mode("Main:timesAllowErrors");

    cout << " -==> Initialization (***)...";
    // Initialize.
    pythia.settings.parm("Beams:eA", 1000.0);
#ifdef WITHGPERFTOOLS
    ProfilerStart("main01-profile.log");
#endif
    pythia.init();
#ifdef WITHGPERFTOOLS
    ProfilerStop();
#endif
    cout << " Ok!\n";
    pythia.next();

    // ================================ (1) =======================================
    // Begin event loop.

    double beamAEnergy = 1000.0;

    pythia.settings.parm("Beams:eA", beamAEnergy);
    pythia.init();
    // pythia.reinitKinematics();

    int iAbort = 0;

    int sec_p_count = 0;
    int sec_n_count = 0;
    int sec_pbar_count = 0;
    int sec_nbar_count = 0;

    double sec_p_summ_energy = 0.0;
    double sec_n_summ_energy = 0.0;
    double sec_pbar_summ_energy = 0.0;
    double sec_nbar_summ_energy = 0.0;

    for (int iEvent = 0; iEvent < nEvents; ++iEvent) {

        // Generate events. Quit if many failures.
        if (!pythia.next()) {
            if (++iAbort < nAbort) continue;
            cout << " Event generation aborted prematurely, owing to error!\n";
            break;
        }

        int eventSize = event.size();

        for (int i = 0; i < eventSize; ++i) {
            if (event[i].isFinal()) {
                switch (event[i].id()) {
                    // secondary protons
                    case 2212:
                    
                        sec_p_summ_energy += event[i].e();
                        sec_p_count += 1;
                        break;

                    // secondady neutrons:
                    case 2112:

                        sec_n_summ_energy += event[i].e();
                        sec_n_count += 1;
                        break;

                    // antiprotons
                    case -2212:
                    
                        sec_pbar_summ_energy += event[i].e();
                        sec_pbar_count += 1;
                        break;

                    // antineutrons:
                    case -2112:

                        sec_nbar_summ_energy += event[i].e();
                        sec_nbar_count += 1;
                        break;

                    default:
                        break;
                }
            }
        }
    }

    std::cout
    	<< "\nBeam energy: " << beamAEnergy << "; events number: " << nEvents
    	<< "\nSecondary protons total: " << sec_p_count
    	<< "; mean energy: " << sec_p_summ_energy / static_cast<double>(sec_p_count)
    	<< "\nSecondary neutrons total: " << sec_n_count
    	<< "; mean energy: " << sec_n_summ_energy / static_cast<double>(sec_n_count)
    	<< "\nSecondary antiprotons total: " << sec_pbar_count
    	<< "; mean energy: " << sec_pbar_summ_energy / static_cast<double>(sec_pbar_count)
    	<< "\nSecondary antineutrons total: " << sec_nbar_count
    	<< "; mean energy: " << sec_nbar_summ_energy / static_cast<double>(sec_nbar_count)
        << "\nSecondary antiprotons number to primary protons number ratio: "
        << static_cast<double>(sec_p_count) / static_cast<double>(nEvents)
        << "\nSecondary antineutrons number to primary protons number ratio: "
        << static_cast<double>(sec_n_count) / static_cast<double>(nEvents)
        << "\nPrimary protons number to secondary antiprotons number ratio: "
        << static_cast<double>(nEvents) / static_cast<double>(sec_pbar_count)
        << "\nPrimary protons number to secondary antineutrons number ratio: "
        << static_cast<double>(nEvents) / static_cast<double>(sec_nbar_count)
        << std::endl;

    // ================================ (2) =======================================
    // Begin event loop.

    beamAEnergy = 6000.0;

    pythia.settings.parm("Beams:eA", beamAEnergy);
    pythia.init();
    // pythia.reinitKinematics();

    iAbort = 0;

    sec_p_count = 0;
    sec_n_count = 0;
    sec_pbar_count = 0;
    sec_nbar_count = 0;

    sec_p_summ_energy = 0.0;
    sec_n_summ_energy = 0.0;
    sec_pbar_summ_energy = 0.0;
    sec_nbar_summ_energy = 0.0;

    for (int iEvent = 0; iEvent < nEvents; ++iEvent) {

        // Generate events. Quit if many failures.
        if (!pythia.next()) {
            if (++iAbort < nAbort) continue;
            cout << " Event generation aborted prematurely, owing to error!\n";
            break;
        }

        int eventSize = event.size();

        for (int i = 0; i < eventSize; ++i) {
            if (event[i].isFinal()) {
                switch (event[i].id()) {
                    // secondary protons
                    case 2212:
                    
                        sec_p_summ_energy += event[i].e();
                        sec_p_count += 1;
                        break;

                    // secondady neutrons:
                    case 2112:

                        sec_n_summ_energy += event[i].e();
                        sec_n_count += 1;
                        break;

                    // antiprotons
                    case -2212:
                    
                        sec_pbar_summ_energy += event[i].e();
                        sec_pbar_count += 1;
                        break;

                    // antineutrons:
                    case -2112:

                        sec_nbar_summ_energy += event[i].e();
                        sec_nbar_count += 1;
                        break;

                    default:
                        break;
                }
            }
        }
    }

    std::cout
    	<< "\nBeam energy: " << beamAEnergy << "; events number: " << nEvents
    	<< "\nSecondary protons total: " << sec_p_count
    	<< "; mean energy: " << sec_p_summ_energy / static_cast<double>(sec_p_count)
    	<< "\nSecondary neutrons total: " << sec_n_count
    	<< "; mean energy: " << sec_n_summ_energy / static_cast<double>(sec_n_count)
    	<< "\nSecondary antiprotons total: " << sec_pbar_count
    	<< "; mean energy: " << sec_pbar_summ_energy / static_cast<double>(sec_pbar_count)
    	<< "\nSecondary antineutrons total: " << sec_nbar_count
    	<< "; mean energy: " << sec_nbar_summ_energy / static_cast<double>(sec_nbar_count)
        << "\nSecondary antiprotons number to primary protons number ratio: "
        << static_cast<double>(sec_p_count) / static_cast<double>(nEvents)
        << "\nSecondary antineutrons number to primary protons number ratio: "
        << static_cast<double>(sec_n_count) / static_cast<double>(nEvents)
        << "\nPrimary protons number to secondary antiprotons number ratio: "
        << static_cast<double>(nEvents) / static_cast<double>(sec_pbar_count)
        << "\nPrimary protons number to secondary antineutrons number ratio: "
        << static_cast<double>(nEvents) / static_cast<double>(sec_nbar_count)
        << std::endl;

    // ================================ (3) =======================================
    // Begin event loop.

    beamAEnergy = 16000.0;

    pythia.settings.parm("Beams:eA", beamAEnergy);
    pythia.init();
    // pythia.reinitKinematics();

    iAbort = 0;

    sec_p_count = 0;
    sec_n_count = 0;
    sec_pbar_count = 0;
    sec_nbar_count = 0;

    sec_p_summ_energy = 0.0;
    sec_n_summ_energy = 0.0;
    sec_pbar_summ_energy = 0.0;
    sec_nbar_summ_energy = 0.0;

    for (int iEvent = 0; iEvent < nEvents; ++iEvent) {

        // Generate events. Quit if many failures.
        if (!pythia.next()) {
            if (++iAbort < nAbort) continue;
            cout << " Event generation aborted prematurely, owing to error!\n";
            break;
        }

        int eventSize = event.size();

        for (int i = 0; i < eventSize; ++i) {
            if (event[i].isFinal()) {
                switch (event[i].id()) {
                    // secondary protons
                    case 2212:
                    
                        sec_p_summ_energy += event[i].e();
                        sec_p_count += 1;
                        break;

                    // secondady neutrons:
                    case 2112:

                        sec_n_summ_energy += event[i].e();
                        sec_n_count += 1;
                        break;

                    // antiprotons
                    case -2212:
                    
                        sec_pbar_summ_energy += event[i].e();
                        sec_pbar_count += 1;
                        break;

                    // antineutrons:
                    case -2112:

                        sec_nbar_summ_energy += event[i].e();
                        sec_nbar_count += 1;
                        break;

                    default:
                        break;
                }
            }
        }
    }

    std::cout
    	<< "\nBeam energy: " << beamAEnergy << "; events number: " << nEvents
    	<< "\nSecondary protons total: " << sec_p_count
    	<< "; mean energy: " << sec_p_summ_energy / static_cast<double>(sec_p_count)
    	<< "\nSecondary neutrons total: " << sec_n_count
    	<< "; mean energy: " << sec_n_summ_energy / static_cast<double>(sec_n_count)
    	<< "\nSecondary antiprotons total: " << sec_pbar_count
    	<< "; mean energy: " << sec_pbar_summ_energy / static_cast<double>(sec_pbar_count)
    	<< "\nSecondary antineutrons total: " << sec_nbar_count
    	<< "; mean energy: " << sec_nbar_summ_energy / static_cast<double>(sec_nbar_count)
        << "\nSecondary antiprotons number to primary protons number ratio: "
        << static_cast<double>(sec_p_count) / static_cast<double>(nEvents)
        << "\nSecondary antineutrons number to primary protons number ratio: "
        << static_cast<double>(sec_n_count) / static_cast<double>(nEvents)
        << "\nPrimary protons number to secondary antiprotons number ratio: "
        << static_cast<double>(nEvents) / static_cast<double>(sec_pbar_count)
        << "\nPrimary protons number to secondary antineutrons number ratio: "
        << static_cast<double>(nEvents) / static_cast<double>(sec_nbar_count)
        << std::endl;
}
