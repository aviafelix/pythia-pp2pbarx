// models.hh

#ifndef __MODELS_HH_INCLUDED__
#define __MODELS_HH_INCLUDED__

#include <string>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

// ****************************
typedef std::map<std::string, double(*)(double)> KnownModelJ;
// ****************************

// Попробовать для хранения настроек:
// std::map<std::string, boost::variant<bool,int,std::string> >

// extern const KnownModelJ known_models;
extern KnownModelJ known_models;

double J_GM75(double);
double J_BH00(double);
double J_WH03(double);
double J_LA03(double);
double J_WH09(double);
double F(double, double, double, double);

#endif
