// higc_clargs.hh

#include <string>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#ifndef __HIGC_CLARGS_HH_INCLUDED__
#define __HIGC_CLARGS_HH_INCLUDED__

namespace po = boost::program_options;

// *******************************
struct TSettings {

    std::string ModelName;
    double WindowWidth;
    bool isWindowWidthRelative;
    unsigned int npoints;
    bool isLogScaleMean;
    bool isLogScaleDots;

    std::string PrimaryProtons;
    std::string SecondaryProtons, SecondaryNeutrons;
    std::string SecondaryAntiprotons, SecondaryAntineutrons;

    std::string outputFileName;

};
// *******************************

bool get_options(int, char* [], TSettings*);
po::variables_map process_options(int, char* []);

#endif
