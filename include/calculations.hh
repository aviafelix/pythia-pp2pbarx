// calculations.hh

#ifndef __CALCULATIONS_HH_INCLUDED__
#define __CALCULATIONS_HH_INCLUDED__

#include <string>
#include <sstream>
#include <cstdlib>
#include "clargs.hh"

// Преобразование в строку для передачи
// числового значения в Pythia (pythia.readString("..."))
// ****************************
template <typename T> std::string to_string(T val)
{
    std::stringstream stream;
    stream << val;
    return stream.str();
}
// ****************************

void test_it(TSettings*);
void gen_linear_scale(TSettings*);
void gen_log_scale(TSettings*);

#endif
