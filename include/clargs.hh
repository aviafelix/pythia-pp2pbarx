// clargs.hh

#include <string>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>

#ifndef __CLARGS_HH_INCLUDED__
#define __CLARGS_HH_INCLUDED__

namespace po = boost::program_options;

// *******************************
struct TSettings {

    std::string ModelName;
    double Emin, Emax;
    int NEvents, TotalSegments, NStripsInSegment, curSegment;
    std::string cmndFileName, outputFilePrefix;

};
// *******************************

bool get_options(int, char* [], TSettings*);
po::variables_map process_options(int, char* []);

// extern TSettings settings;

//  = {
//     "",
//     0.001, 1.0E+03,
//     0,0,0,0,
//     "",
//     "",
// };

#endif
