// higc_funcs.hh

#ifndef __HIGC_FUNCS_HH_INCLUDED__
#define __HIGC_FUNCS_HH_INCLUDED__

void process_data(const TSettings &, bool =false, bool =false);

std::vector<double> calculate_dots(
    double *, size_t, double (*J)(double), unsigned int, double,
    bool =false, bool =false, bool =false
);

std::vector<double> calculate_dots(
    const std::vector<double> &, double (*J)(double), unsigned int, double,
    bool =false, bool =false, bool =false
);

std::vector< double > read_data(const std::string source_file);

std::vector< double > read_data(
    const std::string source_file_1,
    const std::string source_file_2
);

#endif
