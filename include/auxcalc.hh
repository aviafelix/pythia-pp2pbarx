// auxcalc.hh

#ifndef __AUXCALC_HH_INCLUDED__
#define __AUXCALC_HH_INCLUDED__

// ********************
// В этой структуре сохраняются значение интеграла
// и среднего значение величины в интервале энергии
struct TIResult {

    double S, Mean;

};
// ********************

double rand_t(void);
void randomize(void);
double rndm_energy(double, double, double, double, double);
void log_mean_integrate(double (*J)(double), double, double, int, TIResult*);
void mean_integrate(double (*J)(double), double, double, int, TIResult*);
double logintegrate(double (*J)(double), double, double, int);
double integrate(double (*J)(double), double, double, int);
void print_current_time(void);
void print_J_T(double (*J)(double));
void gen_random_E(double, double, double, double, double);
void models_info(void);
void random_sum(void);

#endif
