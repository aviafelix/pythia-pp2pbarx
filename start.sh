#!/usr/bin/env bash
#
TASKNO_FILE=taskno
#
NPROCS=16
MODELNAME=WH03
EMIN=1.0E-03
EMAX=1.0E+03
EVENTS=3000000
SIS=125
# SEGNO=0
SEGNO=${NPROCS}
#
if [ -f ${TASKNO_FILE} ]
then
	TASKNO=$(head -n1 ${TASKNO_FILE})
	TASKNO=$((TASKNO+1))
	echo ${TASKNO}>${TASKNO_FILE}
else
	TASKNO=0
	echo ${TASKNO}>${TASKNO_FILE}
fi
#
while  [ $SEGNO -gt 0 ];
do
	SEGNO=$((SEGNO-1))
	qsub -q pamela -vNPROCS=${NPROCS},MODELNAME=${MODELNAME},EMIN=${EMIN},EMAX=${EMAX},EVENTS=${EVENTS},SIS=${SIS},SEGNO=${SEGNO},WDIR=$(pwd),TASKNO=${TASKNO} -V pbarxc.pbs
	sleep 0.2
done
#
