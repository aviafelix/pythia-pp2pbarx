#include "Pythia8/Pythia.h"
#include <cstdlib>
#include <ctime>
#include <string>
#include <iostream>
#include <math.h>

using namespace Pythia8;

float rand_t() {   return static_cast <float>(rand()) / RAND_MAX;   }

template <typename T> string to_string(T val)
{
    stringstream stream;
    stream << val;
    return stream.str();
}

void randomize(void) {
    // global_urng().seed( time(0)) );
    srand(static_cast <unsigned> (time(0)));
}

double rndm_energy(double _r_gamma, double _E0, double _Emin, double _Emax, double _Phi) {
    double u = rand_t();
    double F = u;
    return _E0 * std::pow(F / _Phi, _r_gamma);
    // return _Emin + (_Emax - _Emin) * std::pow(u, _r_gamma);
}

double r_gamma = -1./2.715;
double E0 = 100.0; // GeV
double Emin = 30.0; // GeV
double Emax = 540.0; // GeV
double Phi = 0.0466; // 4.66 * 10^-2 (m^2 sr s GeV)^-1

int r_e = 0.0;

int main (int argc, char* argv[]) {

    // std::random_device rd;
    // sчто td::mt19937 e2(rd());
    // std::knuth_b e2(rd());

    randomize();

/*
    for (int ccnt = 0; ccnt < 3000; ++ccnt) {

        r_e = rndm_energy(r_gamma, E0, Emin, Emax, Phi);
        // if (r_e > 300) {
        // if (r_e < 30) {
        {
            cout << "cnt: " << ccnt << "; "
                 << r_e << " GeV" << endl;
        }
    }

    return 0;
*/

/*
    float sumx = 0.0;
    int count = 0;
    for (int i = 0; i < 1000; ++i) {
        // cout << sumx << endl;
        sumx += rand_t();
        count += 1;
    }

    cout << sumx/count << endl;
*/

    if (argc != 3) {
        printf("Usage:\n    mymain02 <file.cmnd> <output-file>\n");
        return -1;
    }

    ofstream outputFile;
    outputFile.open(argv[2]);

    // Set up generation
    // Declare Pythia object
    Pythia pythia;

    // Read in command file
    pythia.readFile(argv[1]);


    float beamAEnergy = 0.0;
    beamAEnergy = rndm_energy(r_gamma, E0, Emin, Emax, Phi);
    // cout << " ** 1. Beam A energy: " << beamAEnergy << endl;

    // collision energy
    // pythia.readString("Beams:idA = 2212");
    // pythia.readString("Beams:idB = 2212");
    // pythia.readString("Beams:eA = " + to_string(beamAEnergy));
    // pythia.readString("Beams:eB = 1.");
    // pythia.readString("HardQCD:all = on");
    // pythia.readString("SoftQCD:all = on");
    // pythia.readString("Main:numberOfEvents = 1");
    // pythia.readString("Random:setSeed = on");
    // pythia.readString("Random:seed = 0");
    // pythia.init();

    // Show settings
    // pythia.settings.listChanged();      // Show changed settings
    // pythia.particleData.listChanged();  // Show changed particle data
    
    // Define variables
    // Number of events as defined in .cmnd card file
    // int nEvents = pythia.mode("Main:numberOfEvents");

    // Output file header
    outputFile
        << "collision"
        << ";particle_id"
        << ";motherA_e"
        << ";particle_e"
        << ";particle_px"
        << ";particle_py"
        << ";particle_pz"
        << endl;

    // Loop over rapidity windows
    for (int counter = 0; counter < 1200; ++counter) {
    // for (int counter = 0; counter < 1; ++counter) {
        // Energy of beam A
        beamAEnergy = rndm_energy(r_gamma, E0, Emin, Emax, Phi);
        // beamAEnergy = 7000.0;

        pythia.readString("Beams:eA = " + to_string(beamAEnergy));
        // Initialize as per .cmnd file
        pythia.init();

        int nEvents = pythia.mode("Main:numberOfEvents");
        // cout << " ** Events #: " << nEvents << endl;
        // cout << " *** eA = " << pythia.parm("Beams:eA")
        //      << " GeV"
        //      << endl;

        // Generate events(s)
        // Generate an (other) event. Fill record.
        pythia.next();
        
        // Particle Loop
        for (int i = 0; i < pythia.event.size(); ++i) {
            // Check if it is charged and final
            // if (pythia.event[i].isCharged()&&
            //     pythia.event[i].isFinal()) {

            if ((pythia.event[i].id() == 2212 ||
                pythia.event[i].id() == -2212) &&
                pythia.event[i].isFinal())
            {
                outputFile
                    << counter
                    << ";" << pythia.event[i].id()
                    << scientific
                    << setprecision(5)
                    << ";" << beamAEnergy
                    << ";" << pythia.event[i].e()
                    << ";" << pythia.event[i].px()
                    << ";" << pythia.event[i].py()
                    << ";" << pythia.event[i].pz()
                    << endl;
            }

/*
            // Protons
            if (pythia.event[i].id() == 2212 &&
                pythia.event[i].isFinal())
            {
                cout << "Proton with energy "
                     << pythia.event[i].e()
                     << " GeV"
                     << endl;
            }

            // Antiprotons
            if (pythia.event[i].id() == -2212 &&
                pythia.event[i].isFinal())
                {
                cout << "Antiproton with energy "
                     << pythia.event[i].e()
                     << " GeV"
                     << endl;
            }
*/
        }
/*
        for (int i = 0; i < pythia.event.size(); ++i) {
            cout << "Particle: "
                 << pythia.event[i].name()
                 << "("
                 << pythia.event[i].id()
                 << "); is final?: "
                 << pythia.event[i].isFinal()
                 << "; energy: "
                 << pythia.event[i].e()
                 // << "; status: "
                 // << pythia.event[i].status()
                 << endl;
        }
*/     
 
    }

    outputFile.close();
    return 0;

}

// А.В. прислать файл с результатами.
// http://10.7.129.46/