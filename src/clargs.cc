// clargs.cc

#include <iostream>
#include <string>
#include <iostream>
#include <iomanip>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "models.hh"
#include "clargs.hh"

// ****************************************************
po::variables_map process_options(int argc, char* argv[]) {

    po::options_description desc("General options");

    desc.add_options()
        ("help,h", "Show help")
        ("model,m", po::value<std::string>(),
            "Model name (GM75, BH00, WH03, LA03, WH09, PAMELA)")
        ("e-min", po::value<double>(), "Emin in GeV")
        ("e-max", po::value<double>(), "Emax in GeV")
        ("events-number,N", po::value<int>(),
            "Number of events (primary protons count)")
        ("total-segments,S", po::value<int>(),
            "Number of segments of the energy interval, ~ (Emax-Emin)/total_segments")
        ("strips-in-segment,n", po::value<int>(),
            "Strips number of every segment; total strips number = segments * strips_in_segment")
        ("segment,s", po::value<int>(), "Current segment number")
        ("cmnd-file,c", po::value<std::string>(), ".cmnd file name")
        ("output-file-prefix,o", po::value<std::string>(), "Output file name")
        ;

    static po::variables_map vm;

    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

    }
    catch (std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;

        throw;
    }

    try {
        if (vm.count("help")) {

            std::cout << desc << std::endl;

        }
    }
    catch (std::exception& e) {

        throw;

    }

    return vm;

}
// ****************************************************

// ****************************************************
bool get_options(int argc, char* argv[], TSettings* psettings) {

    po::variables_map vm;
    bool retflag = true;

    try {

        vm = process_options(argc, argv);

    }
    catch (std::exception& e) {

        std::cout << "[ End of prog ]" << std::endl;

        throw;

    }

    if (vm.count("help")) {

        std::cout << "[HELP!]" << std::endl;

        return 1;

    }

    std::cout << std::scientific << std::setprecision(10);

    if (vm.count("model")) {

        psettings->ModelName = vm["model"].as<std::string>();

        // Указано неправильное имя для модели
        if (known_models.find(psettings->ModelName) == known_models.end()) {

            std::cout << "Error: wrong model name: " <<
                psettings->ModelName << std::endl << "Available values are: ";

            for (KnownModelJ::iterator it = known_models.begin(); it != known_models.end(); ++it) {
                std::cout << it->first << " ";
            }
            
            std::cout << std::endl;

            retflag = retflag & false;

        } else {

            std::cout << "Model name is setted to <" << psettings->ModelName <<
                ">" << std::endl;

        }

    } else {

        std::cout << " [!!] Model name is not setted" << std::endl;

        retflag = retflag & false;

    }

    if (vm.count("e-min")) {

        psettings->Emin = vm["e-min"].as<double>();
        std::cout << "Emin is setted to " << psettings->Emin << " GeV" << std::endl;

    } else {

        std::cout << " [!!] The <Emin> value is not setted" << std::endl;

        retflag = retflag & false;
    }

    if (vm.count("e-max")) {
    
        psettings->Emax = vm["e-max"].as<double>();
        std::cout << "Emax is setted to " << psettings->Emax << " GeV" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <Emax> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (vm.count("events-number")) {
    
        psettings->NEvents = vm["events-number"].as<int>();
        std::cout << "Events number is setted to " << psettings->NEvents << std::endl;
    
    } else {
    
        std::cout << " [!!] The <events number> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (vm.count("total-segments")) {
    
        psettings->TotalSegments = vm["total-segments"].as<int>();
        std::cout << "The total number of segments is setted to " <<
            psettings->TotalSegments << std::endl;
    
    } else {
    
        std::cout << " [!!] The <total segments> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (vm.count("strips-in-segment")) {
    
        psettings->NStripsInSegment = vm["strips-in-segment"].as<int>();
        std::cout << "The strips number in segment is setted to " <<
            psettings->NStripsInSegment << std::endl;
    
    } else {
    
        std::cout << " [!!] The <number of strips in segment> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (vm.count("segment")) {
    
        psettings->curSegment = vm["segment"].as<int>();
        std::cout << "The current segment number is setted to " << psettings->curSegment << std::endl;
    
    } else {
    
        std::cout << " [!!] The <current segment number> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (vm.count("cmnd-file")) {
    
        psettings->cmndFileName = vm["cmnd-file"].as<std::string>();
        std::cout << "The .cmnd file name is setted to <" <<
            psettings->cmndFileName << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <.cmnd file name> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (vm.count("output-file-prefix")) {
    
        psettings->outputFilePrefix = vm["output-file-prefix"].as<std::string>();
        std::cout << "The output files prefix is setted to <" <<
            psettings->outputFilePrefix << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <output data file name prefix> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (!retflag) {

        std::cout << "[ Exiting... ]" << std::endl;

    }

    return retflag;

}
// ****************************************************
