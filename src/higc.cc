// higc.cc, main file

#include <ctime>
#include <iostream>
#include <iomanip>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "higc_clargs.hh"
#include "higc_funcs.hh"
#include "higc.hh"

int main (int argc, char* argv[])
{

    TSettings settings;
    // std::cout.scientific;
    // std::cout.setprecision(10);
    std::cout << std::scientific << std::setprecision(10);

    // 
    try
    {
        if (!get_options(argc, argv, &settings)) {
 
            return 0;
 
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl;
        return -1;
    }

    // 
    try
    {
        process_data(settings);
    }
    catch (std::exception& e)
    {
        std::cerr
            << std::endl
            << " error during data procession:"
            << std::endl
            << e.what() << std::endl;

            return -1;
    }

    return 0;
}
