// higc_clargs.cc

#include <iostream>
#include <string>
#include <iostream>
#include <iomanip>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "models.hh"
#include "higc_clargs.hh"

// ****************************************************
po::variables_map process_options(int argc, char* argv[]) {

    po::options_description desc("General options");

    desc.add_options()
        ("help,h", "Show help")
        ("model,m", po::value<std::string>(),
            "Model name (GM75, BH00, WH03 LA03, WH09, PAMELA)")
        ("window-width,w", po::value<double>(),
            "Mean value window width ([GeV] or dimensionless,"
            " if --relative-window-width is switched on)")
        ("relative-window-width,r", po::bool_switch()->default_value(false),
            "Consider the window size as relative value"
            " (with respect to the energy interval)")
        ("n-points,N", po::value<unsigned int>(),
            "Number of points to calculate")
        ("logscale-mean", po::bool_switch()->default_value(false),
            "Calculate mean values logarithmic scale")
        ("logscale-dots,l", po::bool_switch()->default_value(false),
            "Calculate points in logarithmic scale")
        ("primary-protons,P", po::value<std::string>(),
            "Primary protons data file")
        ("secondary-protons,p", po::value<std::string>(),
            "Secondary protons data file")
        ("secondary-neutrons,n", po::value<std::string>(),
            "Primary neutrons data file")
        ("secondary-antiprotons,q", po::value<std::string>(),
            "Primary antiprotons data file")
        ("secondary-antineutrons,s", po::value<std::string>(),
            "Primary antineutrons data file")
        ("output-file,o", po::value<std::string>(), "Output file name")
        ;

    static po::variables_map vm;

    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

    }
    catch (std::exception& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;

        throw;
    }

    try
    {
        if (vm.count("help")) {

            std::cout << desc << std::endl;

        }
    }
    catch (std::exception& e)
    {

        throw;

    }

    return vm;

}
// ****************************************************

// ****************************************************
bool get_options(int argc, char* argv[], TSettings* psettings) {

    po::variables_map vm;
    bool retflag = true;

    try
    {
        vm = process_options(argc, argv);

    }
    catch (std::exception& e)
    {
        std::cout << "[ End of prog ]" << std::endl;

        throw;

    }

    // 
    if (vm.count("help")) {

        std::cout << "[HELP!]" << std::endl;

        return false;

    }

    std::cout << std::scientific << std::setprecision(10);

    // 
    if (vm.count("model")) {

        psettings->ModelName = vm["model"].as<std::string>();

        // Указано неправильное имя для модели
        if (known_models.find(psettings->ModelName) == known_models.end()) {

            std::cout << "Error: wrong model name: " <<
                psettings->ModelName << std::endl << "Available values are: ";

            for (KnownModelJ::iterator it = known_models.begin(); it != known_models.end(); ++it) {

                std::cout << it->first << " ";
            }
            
            std::cout << std::endl;

            retflag = retflag & false;

        } else {

            std::cout << "Model name is setted to <" << psettings->ModelName <<
                ">" << std::endl;
        }

    } else {

        std::cout << " [!!] Model name is not setted" << std::endl;

        retflag = retflag & false;
    }

    // 
    psettings->isWindowWidthRelative = vm["relative-window-width"].as<bool>();
    if (psettings->isWindowWidthRelative) {

        std::cout << "Window width considered as relative value" << std::endl;

    } else {

        std::cout << "Window width considered as absolute value" << std::endl;

    }

    // 
    if (vm.count("window-width")) {

        psettings->WindowWidth = vm["window-width"].as<double>();
        std::cout << "Window width is setted to "
            << psettings->WindowWidth
            << (psettings->isWindowWidthRelative ? "" : " GeV")
            << std::endl;

    } else {

        std::cout << " [!!] Windows width value is not setted" << std::endl;

        retflag = retflag & false;
    }

    // 
    if (vm.count("n-points")) {
    
        psettings->npoints = vm["n-points"].as<unsigned int>();
        std::cout << "The number of points to calculate is setted to <" <<
            psettings->npoints << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The points number value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    // 
    psettings->isLogScaleMean = vm["logscale-mean"].as<bool>();
    if (psettings->isLogScaleMean) {

        std::cout << "Calculating mean values in logarithmic scale is on" << std::endl;

    } else {

        std::cout << "Calculating mean values in logarithmic scale is off" << std::endl;

    }

    // 
    psettings->isLogScaleDots = vm["logscale-dots"].as<bool>();
    if (psettings->isLogScaleDots) {

        std::cout << "Equal distances between points in logarithmic scale" << std::endl;

    } else {

        std::cout << "Equal distances between points in linear scale" << std::endl;

    }

    // 
    if (vm.count("primary-protons")) {
    
        psettings->PrimaryProtons = vm["primary-protons"].as<std::string>();
        std::cout << "The primary protons data file name is setted to <" <<
            psettings->PrimaryProtons << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <primary protons data file name> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    // 
    if (vm.count("secondary-protons")) {
    
        psettings->SecondaryProtons = vm["secondary-protons"].as<std::string>();
        std::cout << "The primary protons data file name is setted to <" <<
            psettings->SecondaryProtons << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <secondary protons data file name> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    // 
    if (vm.count("secondary-neutrons")) {
    
        psettings->SecondaryNeutrons = vm["secondary-neutrons"].as<std::string>();
        std::cout << "The primary neutrons data file name is setted to <" <<
            psettings->SecondaryNeutrons << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <secondary neutrons data file name> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    // 
    if (vm.count("secondary-antiprotons")) {
    
        psettings->SecondaryAntiprotons = vm["secondary-antiprotons"].as<std::string>();
        std::cout << "The primary protons data file name is setted to <" <<
            psettings->SecondaryAntiprotons << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <secondary antiprotons data file name> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    // 
    if (vm.count("secondary-antineutrons")) {
    
        psettings->SecondaryAntineutrons = vm["secondary-antineutrons"].as<std::string>();
        std::cout << "The primary neutrons data file name is setted to <" <<
            psettings->SecondaryAntineutrons << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <secondary antineutrons data file name> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    // 
    if (vm.count("output-file")) {
    
        psettings->outputFileName = vm["output-file"].as<std::string>();
        std::cout << "The output files name is setted to <" <<
            psettings->outputFileName << ">" << std::endl;
    
    } else {
    
        std::cout << " [!!] The <output data file name> value is not setted" << std::endl;
        
        retflag = retflag & false;
    }

    if (!retflag) {

        std::cout << "[ Exiting... ]" << std::endl;

    }

    return retflag;

}

// ****************************************************
