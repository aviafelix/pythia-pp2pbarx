// pp2pbarx.cc, main file

#include <cstdlib>
#include <ctime>
#include <string>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <map>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "Pythia8/Pythia.h"
#include "pp2pbarx.hh"
#include "models.hh"
#include "auxcalc.hh"
#include "calculations.hh"
#include "clargs.hh"
// #include "config.hh"

int main (int argc, char* argv[]) {

    TSettings settings;
    std::cout << std::scientific << std::setprecision(10);

    try {
 
        if (!get_options(argc, argv, &settings)) {
 
            return -1;
 
        }
 
    }
    catch (std::exception& e) {
 
        return -1;
 
    }

    print_current_time();
    randomize();

    // **************************
    test_it(&settings);
    gen_linear_scale(&settings);
    // gen_log_scale(&settings);
    // **************************

    print_current_time();

    return 0;

    // end of main()

}
