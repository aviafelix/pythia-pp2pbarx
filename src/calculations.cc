// calcultions.cc

#include <cstdlib>
#include <ctime>
#include <cmath>
#include <string>
#include <iostream>
#include "Pythia8/Pythia.h"
#include "calculations.hh"
#include "models.hh"
#include "auxcalc.hh"
#include "clargs.hh"

using namespace Pythia8;

static const double Emin_bound = 7.069855; // GeV :: boundary value
static const double M_proton = 0.938272046; // in GeV/c^2 [0.938269998]
static const double M_neutron = 0.939565379; // in GeV/c^2 [0.93957]

void put_header(ofstream& outputFile) {
    // Output file header
    outputFile
        // << "collision"
        // << ";event"
        // << ";particle_id"
        // << ";motherA_e_kin"
        // << ";particle_e_kin"
        // << ";particle_px"
        // << ";particle_py"
        // << ";particle_pz"
        // // << ";particle_m"
        // // << ";particle_m0"
        // << std::endl
        << std::scientific
        << std::setprecision(5);

    return;
}

// *********************************************
void test_it(TSettings* psettings) {

    double (*J)(double) = known_models[psettings->ModelName];

    double Emin = Emin_bound - M_proton; // GeV
    // double Emin = psettings->Emin; // GeV
    double Emax = psettings->Emax; // GeV

    // Количество первичных протонов
    static const long int NEvents = psettings->NEvents;

    // Полное число сегментов, на которое делится интервал энергии
    static const int TotalSegments = psettings->TotalSegments;

    // Число разбиений каждого сегмента на полоски энергий
    // первичных протонов
    static const int NStripsInSegment = psettings->NStripsInSegment;

    // Полное число полосок во всём диапазоне энергий
    static const int TotalStrips = TotalSegments * NStripsInSegment;

    // Номер сегмента, в котором генерируются события,
    //   0 <= curSegment <= (TotalSegments - 1)
    static const int curSegment = psettings->curSegment;

    static int k_min = curSegment * NStripsInSegment;
    static int k_max = (curSegment + 1) * NStripsInSegment;

    double E, dE, rho;
    int sum = 0;
    int ni = 0;

    rho = static_cast<double>(NEvents) / logintegrate(J, Emin, Emax, 32000);

    dE = (Emax - Emin) / TotalStrips;

    std::cout << "dE = " << dE << std::endl;
    
    for (int k = k_min; k < k_max; ++k) {

        E = Emin + (static_cast<double>(k) / static_cast<double>(TotalStrips)) * (Emax - Emin);

        ni = static_cast<int>(
            round(
                rho * logintegrate(J, E, E + dE, 10000)
                // rho * logintegrate(J, E, E + dE, 32000)
            )
        );

        sum += ni;

        std::cout << "k = " << k
            << "; E = " << E
            << "; E + dE = " << E + dE
            << "; ni = " << ni << std::endl;

    }

    std::cout << "Sum: " << sum << std::endl;

    return;
}


// *********************************************
void gen_linear_scale(TSettings* psettings) {

    randomize();

    // Выбираем нужную функцию интенсивности (спектр) протонов
    // в зависимости от модели
    double (*J)(double) = known_models[psettings->ModelName];
    // J = &J_GM75;

    double Emin = Emin_bound - M_proton; // kinetic energy in GeV
    // double Emin = psettings->Emin; // kinetic energy in GeV
    double Emax = psettings->Emax; // kinetic energy in GeV

    // Количество первичных протонов
    static const long int NEvents = psettings->NEvents;

    // Полное число сегментов, на которое делится интервал энергии
    static const int TotalSegments = psettings->TotalSegments;

    // Число разбиений каждого сегмента на полоски энергий
    // первичных протонов
    static const int NStripsInSegment = psettings->NStripsInSegment;

    // Полное число полосок во всём диапазоне энергий
    static const int TotalStrips = TotalSegments * NStripsInSegment;

    // Номер сегмента, в котором генерируются события,
    //   0 <= curSegment <= (TotalSegments - 1)
    static const int curSegment = psettings->curSegment;

    static int k_min = curSegment * NStripsInSegment;
    static int k_max = (curSegment + 1) * NStripsInSegment;

    double E, dE, rho;
    int sum = 0;
    int ni = 0;

    rho = static_cast<double>(NEvents) / logintegrate(J, Emin, Emax, 32000);

    // Set up generation
    // Declare Pythia object
    Pythia pythia;
    TIResult res;

    // Read in command file
    pythia.readFile(psettings->cmndFileName);

    // float beamAEnergy = 0.0;
    float beamAEnergy;
    // beamAEnergy = rndm_energy(r_gamma, E0, Emin, Emax, Phi);

    // std::cout << " ** 1. Beam A energy: " << beamAEnergy << std::endl;

    // collision energy
    // pythia.readString("Beams:idA = 2212");
    // pythia.readString("Beams:idB = 2212");
    // pythia.readString("Beams:eA = " + to_string(beamAEnergy));
    // pythia.readString("Beams:eB = 1.");
    // pythia.readString("HardQCD:all = on");
    // pythia.readString("SoftQCD:all = on");
    // pythia.readString("Main:numberOfEvents = 1");
    // pythia.readString("Random:setSeed = on");
    // pythia.readString("Random:seed = 0");
    // pythia.init();

    // Show settings
    // pythia.settings.listChanged();      // Show changed settings
    // pythia.particleData.listChanged();  // Show changed particle data
    
    // Define variables
    // Number of events as defined in .cmnd card file
    // int nEvents = pythia.mode("Main:numberOfEvents");

    dE = (Emax - Emin) / TotalStrips;

    std::cout << "dE = " << dE << std::endl;

    ofstream outputFile_p_prim, outputFile_p, outputFile_pbar, outputFile_n, outputFile_nbar;

    // Сюда складываются все первичные протоны
    outputFile_p_prim.open((psettings->outputFilePrefix+"_lin_p_prim.dat").c_str());

    // А сюда -- вторичные протоны, антипротоны, нейтроны, антинейтроны
    outputFile_p.open((psettings->outputFilePrefix+"_lin_p.dat").c_str());
    outputFile_pbar.open((psettings->outputFilePrefix+"_lin_pbar.dat").c_str());
    outputFile_n.open((psettings->outputFilePrefix+"_lin_n.dat").c_str());
    outputFile_nbar.open((psettings->outputFilePrefix+"_lin_nbar.dat").c_str());

    outputFile_p_prim
    //     << "collision"
    //     << ";event"
    //     << ";particle_e_kin"
    //     << std::endl
        << std::scientific
        << std::setprecision(5);

    put_header(outputFile_p);
    put_header(outputFile_pbar);
    put_header(outputFile_n);
    put_header(outputFile_nbar);

    for (int k = k_min; k < k_max; ++k) {

        E = Emin + (static_cast<double>(k) / static_cast<double>(TotalStrips)) * (Emax - Emin);

        log_mean_integrate(known_models[psettings->ModelName], E, E + dE, 32000, &res);
        // log_mean_integrate(known_models[settings->ModelName], E, E + dE, 10000, &res);

        // Количество генерируемых протонов с данной энергией
        ni = static_cast<int>(round( rho * res.S ));

        sum += ni;

        // Energy of beam A
        // beamAEnergy = rndm_energy(r_gamma, E0, Emin, Emax, Phi);
        // beamAEnergy = 7000.0;
        beamAEnergy = res.Mean + M_proton;

        std::cout << "k = " << k
            << "\n; E = " << E
            << "GeV\n; E + dE = " << E + dE
            << "GeV\n; Emean = " << res.Mean
            << "GeV\n; beamA_Emean = " << beamAEnergy
            << "GeV\n; ni (events) = " << ni
            << "\n; sum (events) = " << sum
            << std::endl << std::endl;

        E = res.Mean;

        // if protons energy is less than boundary
        // for Pythia (where them are not decayed)
        // then just output right amount of p's into
        // outputFile_p and continue
        if (beamAEnergy <= Emin_bound) {

            for (int event = 0; event < ni; ++event) {
                outputFile_p
                    << k
                    << ";" << event
                    << ";2212"
                    // << std::scientific
                    // << std::setprecision(5)
                    << ";" << E // == beamAEnergy - M_proton
                    << ";" << E // == beamAEnergy - M_proton
                    << ";" << 0.0
                    << ";" << 0.0
                    << ";" << 0.0
                    << std::endl;

                outputFile_p_prim
                    << k
                    << ";" << event
                    << ";" << E
                    << std::endl;

            }

            continue;

        }

        if (ni == 0) {

            continue;

        }

        pythia.readString("Beams:eA = " + to_string(beamAEnergy));
        // Initialize as per .cmnd file
        pythia.init();

        // std::cout << " *** eA = " << pythia.parm("Beams:eA")

        // Generate events(s)
        for (int event = 0; event < ni; ++event) {

            outputFile_p_prim
                << k
                << ";" << event
                << ";" << E
                << std::endl;

            // Generate an (other) event. Fill record.
            pythia.next();
            
            // Particle Loop
            for (int i = 0; i < pythia.event.size(); ++i) {
                // Check if it is charged and final
                // if (pythia.event[i].isCharged()&&
                //     pythia.event[i].isFinal()) {

                // If particle is final state
                if (pythia.event[i].isFinal()) {

                    switch (pythia.event[i].id()) {

                        // Protons (p)
                        case 2212:
                            
                            outputFile_p
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // Neutrons (n)
                        case 2112:
                            
                            outputFile_n
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // Antineutrons (nbar)
                        case -2112:
                            
                            outputFile_nbar
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // Antiprotons (pbar)
                        case -2212:
                            
                            outputFile_pbar
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // None of them
                        default:
                            break;

                    }

                }

           }

       }

    }

    outputFile_nbar.close();
    outputFile_n.close();
    outputFile_pbar.close();
    outputFile_p.close();

    outputFile_p_prim.close();

    return;
}

// *********************************************
void gen_log_scale(TSettings* psettings) {

    randomize();

    // Выбираем нужную функцию интенсивности (спектр) протонов
    // в зависимости от модели
    double (*J)(double) = known_models[psettings->ModelName];
    // J = &J_GM75;

    double Emin = Emin_bound - M_proton; // kinetic energy in GeV
    // double Emin = psettings->Emin; // kinetic energy in GeV
    double Emax = psettings->Emax; // kinetic energy in GeV
    double ximin = std::log(psettings->Emin); // kinetic energy in GeV
    double ximax = std::log(psettings->Emax); // kinetic energy in GeV

    // Количество первичных протонов
    static const long int NEvents = psettings->NEvents;

    // Полное число сегментов, на которое делится интервал энергии
    static const int TotalSegments = psettings->TotalSegments;

    // Число разбиений каждого сегмента на полоски энергий
    // первичных протонов
    static const int NStripsInSegment = psettings->NStripsInSegment;

    // Полное число полосок во всём диапазоне энергий
    static const int TotalStrips = TotalSegments * NStripsInSegment;

    // Номер сегмента, в котором генерируются события,
    //   0 <= curSegment <= (TotalSegments - 1)
    static const int curSegment = psettings->curSegment;

    static int k_min = curSegment * NStripsInSegment;
    static int k_max = (curSegment + 1) * NStripsInSegment;

    double E, Enext;
    double xi, dxi, rho;
    int sum = 0;
    int ni = 0;

    rho = static_cast<double>(NEvents) / logintegrate(J, Emin, Emax, 32000);

    // Set up generation
    // Declare Pythia object
    Pythia pythia;
    TIResult res;

    // Read in command file
    pythia.readFile(psettings->cmndFileName);

    // float beamAEnergy = 0.0;
    float beamAEnergy;
    // beamAEnergy = rndm_energy(r_gamma, E0, Emin, Emax, Phi);

    // std::cout << " ** 1. Beam A energy: " << beamAEnergy << std::endl;

    // collision energy
    // pythia.readString("Beams:idA = 2212");
    // pythia.readString("Beams:idB = 2212");
    // pythia.readString("Beams:eA = " + to_string(beamAEnergy));
    // pythia.readString("Beams:eB = 1.");
    // pythia.readString("HardQCD:all = on");
    // pythia.readString("SoftQCD:all = on");
    // pythia.readString("Main:numberOfEvents = 1");
    // pythia.readString("Random:setSeed = on");
    // pythia.readString("Random:seed = 0");
    // pythia.init();

    // Show settings
    // pythia.settings.listChanged();      // Show changed settings
    // pythia.particleData.listChanged();  // Show changed particle data
    
    // Define variables
    // Number of events as defined in .cmnd card file
    // int nEvents = pythia.mode("Main:numberOfEvents");

    dxi = (std::log(Emax) - std::log(Emin)) / TotalStrips;

    std::cout << "dxi = " << dxi << std::endl;

    ofstream outputFile_p_prim, outputFile_p, outputFile_pbar, outputFile_n, outputFile_nbar;

    // Сюда складываются все первичные протоны
    outputFile_p_prim.open((psettings->outputFilePrefix+"_log_p_prim.dat").c_str());

    // А сюда -- вторичные протоны, антипротоны, нейтроны, антинейтроны
    outputFile_p.open((psettings->outputFilePrefix+"_log_p.dat").c_str());
    outputFile_pbar.open((psettings->outputFilePrefix+"_log_pbar.dat").c_str());
    outputFile_n.open((psettings->outputFilePrefix+"_log_n.dat").c_str());
    outputFile_nbar.open((psettings->outputFilePrefix+"_log_nbar.dat").c_str());

    // outputFile_p_prim
    //     << "collision"
    //     << ";event"
    //     << ";particle_e_kin"
    //     << std::endl
    //     << std::scientific
    //     << std::setprecision(5);

    // put_header(outputFile_p);
    // put_header(outputFile_pbar);
    // put_header(outputFile_n);
    // put_header(outputFile_nbar);

    for (int k = k_min; k < k_max; ++k) {

        xi = ximin + (static_cast<double>(k) / static_cast<double>(TotalStrips)) * (ximax - ximin);
        E = std::exp(xi);
        Enext = E * std::exp((ximax - ximin) / static_cast<double>(TotalStrips));

        log_mean_integrate(known_models[psettings->ModelName], E, Enext, 32000, &res);
        // log_mean_integrate(known_models[settings->ModelName], E, E + dE, 10000, &res);

        // Количество генерируемых протонов с данной энергией
        ni = static_cast<int>(round( rho * res.S ));

        sum += ni;

        // Energy of beam A
        // beamAEnergy = rndm_energy(r_gamma, E0, Emin, Emax, Phi);
        // beamAEnergy = 7000.0;
        beamAEnergy = res.Mean + M_proton;

        std::cout << "k = " << k
            << "\n; E = " << E
            << "GeV\n; E + dE = " << Enext
            << "GeV\n; Emean = " << res.Mean
            << "GeV\n; beamA_Emean = " << beamAEnergy
            << "GeV\n; ni (events) = " << ni
            << "\n; sum (events) = " << sum
            << std::endl << std::endl;

        E = res.Mean;

        // if protons energy is less than boundary
        // for Pythia (where them are not decayed)
        // then just output right amount of p's into
        // outputFile_p and continue
        if (beamAEnergy <= Emin_bound) {

            for (int event = 0; event < ni; ++event) {
                outputFile_p
                    << k
                    << ";" << event
                    << ";2212"
                    // << std::scientific
                    // << std::setprecision(5)
                    << ";" << E // == beamAEnergy - M_proton
                    << ";" << E // == beamAEnergy - M_proton
                    << ";" << 0.0
                    << ";" << 0.0
                    << ";" << 0.0
                    << std::endl;

                outputFile_p_prim
                    << k
                    << ";" << event
                    << ";" << E
                    << std::endl;

            }

            continue;

        }

        if (ni == 0) {

            continue;

        }

        pythia.readString("Beams:eA = " + to_string(beamAEnergy));
        // Initialize as per .cmnd file
        pythia.init();

        // std::cout << " *** eA = " << pythia.parm("Beams:eA")

        // Generate events(s)
        for (int event = 0; event < ni; ++event) {

            outputFile_p_prim
                << k
                << ";" << event
                << ";" << E
                << std::endl;

            // Generate an (other) event. Fill record.
            pythia.next();
            
            // Particle Loop
            for (int i = 0; i < pythia.event.size(); ++i) {
                // Check if it is charged and final
                // if (pythia.event[i].isCharged()&&
                //     pythia.event[i].isFinal()) {

                // If particle is final state
                if (pythia.event[i].isFinal()) {

                    switch (pythia.event[i].id()) {

                        // Protons (p)
                        case 2212:
                            
                            outputFile_p
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // Neutrons (n)
                        case 2112:
                            
                            outputFile_n
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // Antineutrons (nbar)
                        case -2112:
                            
                            outputFile_nbar
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // Antiprotons (pbar)
                        case -2212:
                            
                            outputFile_pbar
                                << k
                                << ";" << event
                                << ";" << pythia.event[i].id()
                                // << std::scientific
                                // << std::setprecision(5)
                                << ";" << E // == beamAEnergy - M_proton
                                << ";" << pythia.event[i].e() - pythia.event[i].m()
                                << ";" << pythia.event[i].px()
                                << ";" << pythia.event[i].py()
                                << ";" << pythia.event[i].pz()
                                // << ";" << pythia.event[i].m()
                                // << ";" << pythia.event[i].m0()
                                << std::endl;

                            break;

                        // None of them
                        default:
                            break;

                    }

                }

           }

       }

    }

    outputFile_nbar.close();
    outputFile_n.close();
    outputFile_pbar.close();
    outputFile_p.close();

    outputFile_p_prim.close();

    return;
}
