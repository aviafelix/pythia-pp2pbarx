// models.cc

#include <cstdlib>
#include <cmath>
#include "models.hh"

// **********************************
static const KnownModelJ::value_type known_models_[] = {
    KnownModelJ::value_type("GM75", &J_GM75),
    KnownModelJ::value_type("BH00", &J_BH00),
    KnownModelJ::value_type("WH03", &J_WH03),
    KnownModelJ::value_type("LA03", &J_LA03),
    KnownModelJ::value_type("WH09", &J_WH09),
    KnownModelJ::value_type("PAMELA", &J_GM75),
};

//////////////////////////////////////
KnownModelJ known_models(known_models_,
    known_models_ + sizeof(known_models_) / sizeof(known_models_[0]));
//////////////////////////////////////

/******************************
*   1. GM75 model
*/
// ****************************
// This is differential intensity [flux]
// of dim. part. / (cm^2 sr s GeV)
double J_GM75(double T_GeV) {

    // ****************************
    // static const double a = 9.9E+08; // for [J] = m^-2 sr s MeV/nucl.
    static const double a = 9.9E+07; // for [J] = сm^-2 sr s GeV/nucl.
    static const double b = 780.0;
    static const double c = -2.5E-04;
    // ****************************

    // Convert to MeV/c^2
    double TT_MeV = 1000 * T_GeV;

    return a * std::pow(
        TT_MeV + b * std::exp(c * TT_MeV),
        -2.65
    );
}
// **************************** // GM75

/******************************
2. BH00 model [US05?!]
*/
// ****************************
// This is differential intensity [flux]
// of dim. part. / (cm^2 sr s GeV)
double J_BH00(double T_GeV) {

    // ****************************
    // static const double a = 415.7; // for [J] = m^-2 sr s MeV/nucl.
    static const double a = 41.57; // for [J] = сm^-2 sr s GeV/nucl.
    static const double b = 1.0E-07;
    static const double c = 1.6488;
    static const double M_p = 938.272046; // MeV/c^2
    // ****************************

    // Convert to MeV/c^2
    double TT_MeV = 1000 * T_GeV;

    return a / (
        b * std::pow(
            TT_MeV * (TT_MeV + 2.0 * M_p),
            1.39) +
        c * std::pow(
            TT_MeV * (TT_MeV + 2.0 * M_p),
            0.135)
    );
}
// **************************** // BH00 \ US05

/******************************
// 3. WH03 model
*/
// ****************************
// This is differential intensity [flux]
// of dim. part. / (cm^2 sr s GeV)
double J_WH03(double T_GeV) {

    // ****************************
    // static const double a = 530.0; // for [J] = m^-2 sr s MeV/nucl.
    static const double a = 53.0; // for [J] = cm^-2 sr s GeV/nucl.
    static const double b = 1.0E-07;
    static const double c = 2.674E-03;
    static const double d = 4.919;
    // ****************************

    // Convert to MeV/c^2
    double TT_MeV = 1000 * T_GeV;

    return a / (
        b * std::pow(TT_MeV, 2.8) +
        c * std::pow(TT_MeV, 1.58) +
        d * std::pow(TT_MeV, 0.26)
    );
}
// **************************** // WH03

/******************************
//   4. LA03 model
*/
// ****************************
// This is differential intensity [flux]
// of dim. part. / (cm^2 sr s GeV)
double J_LA03(double T_GeV) {

    // ****************************
    static const double a = 0.823;
    static const double b = 0.08;
    static const double c = 1.105;
    static const double d = 9.202E-02;
    static const double e = 22.976;
    static const double f = 2.86;
    static const double g = 1.5E+03;
    // ****************************

    // Convert to MeV/c^2
    double TT_MeV = 1000 * T_GeV;
    double lnT = std::log(TT_MeV);

    if (TT_MeV < 1000.) {

        return 0.1 * std::exp( // 0.1 for [J] = cm^-2 sr s GeV/nucl.
            a -
            b * lnT * lnT +
            c * lnT -
            d * sqrt(TT_MeV)
        );

    } else {

        return 0.1 * std::exp( // 0.1 for [J] = cm^-2 sr s GeV/nucl.
            e -
            f * lnT -
            g / TT_MeV
        );

    }
}
// **************************** // LA03

/******************************
//   5. WH09 model
*/
// ****************************
// This is differential intensity [flux]
// of dim. part. / (cm^2 sr s GeV)
double J_WH09(double T_GeV) {

    // ****************************
    // T < 1000 MeV:
    static const double a1 = -124.47673;
    static const double b1 = -51.83897;
    static const double c1 = 131.64886;
    static const double d1 = -241.72524;
    static const double e1 = 376.65906;
    // T >= 1000 MeV:
    // static const double a2 = 0;
    static const double b2 = -51.68612;
    static const double c2 = 103.58884;
    static const double d2 = -709.70735;
    static const double e2 = 1161.55701;
    // ****************************

    // Convert to MeV/c^2
    double TT_MeV = 1000 * T_GeV;
    double lnT = std::log(TT_MeV);
    double lnlnT = std::log(lnT);

    if (TT_MeV < 1000.) {

        return 0.1 * std::exp( // 0.1 for [J] = cm^-2 sr s GeV/nucl.
            a1 +
            b1 * lnlnT * lnlnT +
            c1 * sqrt(lnT) +
            d1 / lnT +
            e1 / (lnT * lnT)
        );

    } else {

        return 0.1 * std::exp( // 0.1 for [J] = cm^-2 sr s GeV/nucl.
            // a2 +
            b2 * lnlnT * lnlnT +
            c2 * sqrt(lnT) +
            d2 / lnT +
            e2 / (lnT * lnT)
        );

    }

}
// **************************** // WH09

double F(double _Ek, double _E0, double _Phi, double _gamma) {

    return _Phi * std::pow(_Ek / _E0, _gamma);

}
// ****************************
