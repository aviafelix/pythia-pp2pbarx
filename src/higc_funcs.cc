// higc_funcs.cc

#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
// https://github.com/aviafelix/fast-cpp-csv-parser
#include "fast-cpp-csv-parser/csv.h"
#include "models.hh"
#include "auxcalc.hh"
#include "higc_clargs.hh"
#include "higc_funcs.hh"

// 
double MinValue(const std::vector< double > &);
double MinValue(double *, size_t);
double MaxValue(const std::vector< double > &);
double MaxValue(double *, size_t);
void MinMaxValues(const std::vector< double > &, double &, double &);
void MinMaxValues(double *, size_t, double &, double &);

std::vector<double> calculate_dots(double *data, size_t count, double (*J)(double),
    unsigned int npoints, double WindowWidth, bool logscale_mean, bool logscale_dots,
    bool isWindowWidthRelative);

std::vector<double> calculate_dots(const std::vector<double> &data, double (*J)(double),
    unsigned int npoints, double WindowWidth, bool logscale_mean, bool logscale_dots,
    bool isWindowWidthRelative);

// 
static const double Emin_bound = 7.069855; // GeV :: boundary value
static const double M_proton = 0.938272046; // in GeV/c^2 [0.938269998]
static const double M_neutron = 0.939565379; // in GeV/c^2 [0.93957]

// 
void process_data(const TSettings &settings, bool logscale_mean, bool logscale_dots)
{
    double (*J)(double) = known_models[settings.ModelName];

    // Размер окна может задаваться
    // относительным или абсолютным значением
    double WindowWidth = settings.WindowWidth;
    double emin, emax;

    std::cout << std::scientific << std::setprecision(10);

    // ***
    std::cout
        << " * reading file: "
        << settings.PrimaryProtons
        << "...";
    std::vector< double > energy_values = read_data(settings.PrimaryProtons);

    std::cout
        << " ok!"
        << std::endl
        << " data size: "
        << energy_values.size()
        << std::endl;

    MinMaxValues(energy_values, emin, emax);

    // Интеграл от точки, в которой начинают рождаться протоны,
    // до максимального значения
    double S = logintegrate(J, std::max(emin, Emin_bound), emax, 32000);

    std::cout
        << " min: "
        << MinValue(energy_values)
        << " ("
        << emin
        << ") "
        << std::endl
        << " max: "
        << MaxValue(energy_values)
        << " ("
        << emax
        << ") "
        << std::endl
        << "Integral: "
        << S
        << std::endl;

    // delete [] energy_values;
    std::cout
        << " clearing data...";
    energy_values.clear();
    std::vector< double >().swap(energy_values);   // clear reallocating
    std::cout
        << " ok!"
        << std::endl;
    // std::vector<double>(energy_values).swap(energy_values);

    // ***
    std::cout
        << " * reading file: "
        << settings.SecondaryProtons
        << "...";
    energy_values = read_data(settings.SecondaryProtons);

    std::cout
        << " ok!"
        << std::endl
        << " data size: "
        << energy_values.size()
        << std::endl;

    MinMaxValues(energy_values, emin, emax);

    std::cout
        << " min: "
        << MinValue(energy_values)
        << " ("
        << emin
        << ")"
        << std::endl
        << " max: "
        << MaxValue(energy_values)
        << " ("
        << emax
        << ")"
        << std::endl;

    // delete [] energy_values;
    std::cout
        << " clearing data...";
    energy_values.clear();
    std::vector< double >().swap(energy_values);   // clear reallocating
    std::cout
        << " ok!"
        << std::endl;
    // std::vector<double>(energy_values).swap(energy_values);

    // ***
    std::cout
        << " * reading file: "
        << settings.SecondaryNeutrons
        << "...";
    energy_values = read_data(settings.SecondaryNeutrons);

    std::cout
        << " ok!"
        << std::endl
        << " data size: "
        << energy_values.size()
        << std::endl;

    MinMaxValues(energy_values, emin, emax);

    std::cout
        << " min: "
        << MinValue(energy_values)
        << " ("
        << emin
        << ")"
        << std::endl
        << " max: "
        << MaxValue(energy_values)
        << " ("
        << emax
        << ")"
        << std::endl;

    // delete [] energy_values;
    std::cout
        << " clearing data...";
    energy_values.clear();
    std::vector< double >().swap(energy_values);   // clear reallocating
    std::cout
        << " ok!"
        << std::endl;
    // std::vector<double>(energy_values).swap(energy_values);

    // ***
    std::cout
        << " * reading file: "
        << settings.SecondaryAntiprotons
        << "...";
    energy_values = read_data(settings.SecondaryAntiprotons);

    std::cout
        << " ok!"
        << std::endl
        << " data size: "
        << energy_values.size()
        << std::endl;

    MinMaxValues(energy_values, emin, emax);

    std::cout
        << " min: "
        << MinValue(energy_values)
        << " ("
        << emin
        << ")"
        << std::endl
        << " max: "
        << MaxValue(energy_values)
        << " ("
        << emax
        << ")"
        << std::endl;

    // delete [] energy_values;
    std::cout
        << " clearing data...";
    energy_values.clear();
    std::vector< double >().swap(energy_values);   // clear reallocating
    std::cout
        << " ok!"
        << std::endl;
    // std::vector<double>(energy_values).swap(energy_values);

    // ***
    std::cout
        << " * reading file: "
        << settings.SecondaryAntineutrons
        << "...";
    energy_values = read_data(settings.SecondaryAntineutrons);

    std::cout
        << " ok!"
        << std::endl
        << " data size: "
        << energy_values.size()
        << std::endl;

    MinMaxValues(energy_values, emin, emax);

    std::cout
        << " min: "
        << MinValue(energy_values)
        << " ("
        << emin
        << ")"
        << std::endl
        << " max: "
        << MaxValue(energy_values)
        << " ("
        << emax
        << ")"
        << std::endl;

    // delete [] energy_values;
    std::cout
        << " clearing data...";
    energy_values.clear();
    std::vector< double >().swap(energy_values);   // clear reallocating
    std::cout
        << " ok!"
        << std::endl;
    // std::vector<double>(energy_values).swap(energy_values);

    // ***
    std::cout
        << " * reading files [p + n]: "
        << settings.SecondaryProtons
        << " "
        << settings.SecondaryNeutrons
        << "...";
    energy_values = read_data(settings.SecondaryProtons, settings.SecondaryNeutrons);

    std::cout
        << " ok!"
        << std::endl
        << " data size: "
        << energy_values.size()
        << std::endl;

    MinMaxValues(energy_values, emin, emax);

    std::cout
        << " min: "
        << MinValue(energy_values)
        << " ("
        << emin
        << ")"
        << std::endl
        << " max: "
        << MaxValue(energy_values)
        << " ("
        << emax
        << ")"
        << std::endl;

    // delete [] energy_values;
    std::cout
        << " clearing data...";
    energy_values.clear();
    std::vector< double >().swap(energy_values);   // clear reallocating
    std::cout
        << " ok!"
        << std::endl;
    // std::vector<double>(energy_values).swap(energy_values);

    // ***
    std::cout
        << " * reading files [pbar + nbar]: "
        << settings.SecondaryAntiprotons
        << " "
        << settings.SecondaryAntineutrons
        << "...";
    energy_values = read_data(
        settings.SecondaryAntiprotons,
        settings.SecondaryAntineutrons
    );

    std::cout
        << " ok!"
        << std::endl
        << " data size: "
        << energy_values.size()
        << std::endl;

    MinMaxValues(energy_values, emin, emax);

    std::cout
        << " min: "
        << MinValue(energy_values)
        << " ("
        << emin
        << ")"
        << std::endl
        << " max: "
        << MaxValue(energy_values)
        << " ("
        << emax
        << ")"
        << std::endl;

    // delete [] energy_values;
    std::cout
        << " clearing data...";
    energy_values.clear();
    std::vector< double >().swap(energy_values);   // clear reallocating
    std::cout
        << " ok!"
        << std::endl;
    // std::vector<double>(energy_values).swap(energy_values);

}

// 
std::vector< double > read_data(const std::string source_file)
{
    io::CSVReader<
        1,
        io::trim_chars< ' ', '\t' >,
        io::no_quote_escape< ';' >
    > in_file( source_file );

    // TODO:: сделать правильно.
    // ПОка что приходится вычитать единицу из общего числа строк
    // и затем сравнивать индексную переменнцую из цикла с размером
    unsigned int linescount = in_file.get_number_of_lines() - 1;

    in_file.read_header( io::ignore_extra_column, "particle_e_kin" );

    // Возвращается количество строк без учёта заголовка
    std::vector< double > energy_values = std::vector< double >(linescount);
    std::cout << " ///[" << linescount << "]/// " << std::endl;

    double particle_e_kin;

    {
        size_t k = 0;

        while ( in_file.read_row(particle_e_kin) && (k < linescount) )
        {
            // (*energy_values)[k++] = particle_e_kin;
            energy_values[k++] = particle_e_kin;
        }
    }

    return energy_values;
}

// 
std::vector< double > read_data(
    const std::string source_file_1,
    const std::string source_file_2
)
{
    io::CSVReader<
        1,
        io::trim_chars< ' ', '\t' >,
        io::no_quote_escape< ';' >
    > in_file_1( source_file_1 );

    io::CSVReader<
        1,
        io::trim_chars< ' ', '\t' >,
        io::no_quote_escape< ';' >
    > in_file_2( source_file_2 );

    // TODO:: сделать правильно.
    // ПОка что приходится вычитать единицу из общего числа строк
    // и затем сравнивать индексную переменнцую из цикла с размером
    // ---
    // Возвращается количество строк без учёта заголовка
    unsigned int linescount_1 = in_file_1.get_number_of_lines() - 1;
    unsigned int linescount_2 = in_file_2.get_number_of_lines() - 1;

    in_file_1.read_header( io::ignore_extra_column, "particle_e_kin" );
    in_file_2.read_header( io::ignore_extra_column, "particle_e_kin" );

    std::vector< double > energy_values = std::vector< double >(linescount_1 + linescount_2);

    double particle_e_kin;

    {
        size_t k = 0;

        while ( in_file_1.read_row(particle_e_kin) && (k < linescount_1) )
        {
            // (*energy_values)[k++] = particle_e_kin;
            energy_values[k++] = particle_e_kin;
        }

        while ( in_file_2.read_row(particle_e_kin) && (k < linescount_1 + linescount_2) )
        {
            // (*energy_values)[k++] = particle_e_kin;
            energy_values[k++] = particle_e_kin;
        }
    }

    return energy_values;
}

// 
std::vector< double > find_means(const std::vector< double > &data,
                                 double IntegralVal,
                                 unsigned int npoints,
                                 double InputWindowWidth,
                                 bool isWindowWidthRelative =false)
{
    double emin, emax, WindowWidth;
    MinMaxValues(data, emin, emax);

    if (isWindowWidthRelative) {

        WindowWidth = InputWindowWidth * (emax - emin);
        std::cout
            << " WindowWidth (relative): "
            << WindowWidth
            << " GeV"
            << std::endl;

    } else {

        WindowWidth = InputWindowWidth;
        std::cout
            << " WindowWidth (absolute scale): "
            << WindowWidth
            << " GeV"
            << std::endl;
    }

    std::vector< double > rmean = std::vector< double >(npoints);

    // Цикл по точкам в интервале энергий
    for (unsigned int i = 0; i < npoints; ++i)
    {
        // data[i]
        ;
    }

    // std::cout
    //     << " count values: "
    //     << data.size()
    //     << std::endl;

    return rmean;
}

//////////////////////////////////////////////////////////////////////////////////////
// 
double MinValue(const std::vector< double > &data)
{
    std::vector< double >::const_iterator cit = data.begin();
    std::vector< double >::const_iterator ce = data.end();

    double tmp = *cit;

    // Итерация начинается с первого (0+1) элемента
    for (++cit; cit != ce; ++cit)
    {
        tmp = std::min(tmp, *cit);
    }

    return tmp;
}

// 
double MinValue(double *data, size_t count)
{
    double tmp = data[0];

    for (size_t k = 1; k < count; ++k)
    {
        tmp = std::min(data[k], tmp);
    }

    return tmp;
}

double MaxValue(const std::vector< double > &data)
{
    std::vector< double >::const_iterator cit = data.begin();
    std::vector< double >::const_iterator ce = data.end();

    double tmp = *cit;

    // Итерация начинается с первого (0+1) элемента
    for (++cit; cit != ce; ++cit)
    {
        tmp = std::max(tmp, *cit);
    }

    return tmp;
}

// 
double MaxValue(double *data, size_t count)
{
    double tmp = data[0];

    for (size_t k = 1; k < count; ++k)
    {
        tmp = std::max(data[k], tmp);
    }

    return tmp;
}

// Минимальные и максимальные значения для вектора
void MinMaxValues(const std::vector< double > &data, double &min, double &max)
{
    double tmp = data[0];
    // double tmp = (data*)[0];
    double _min = tmp;
    double _max = tmp;

    for (std::vector< double >::const_iterator cit = data.begin(), ce = data.end();
        cit != ce; ++cit)
    {
        tmp = *cit;
        _min = std::min(tmp, _min);
        _max = std::max(tmp, _max);
    }

    min = _min;
    max = _max;

    return;
}

// Минимальные и максимальные значения для массива
void MinMaxValues(double *data, size_t count, double &min, double &max)
{
    double tmp = data[0];
    double _min = tmp;
    double _max = tmp;

    for (size_t k = 1; k < count; ++k)
    {
        tmp = data[k];
        _min = std::min(tmp, _min);
        _max = std::max(tmp, _max);
    }

    min = _min;
    max = _max;

    return;
}
