// auxcalc.cc

#include <cstdlib>
#include <ctime>
#include <cmath>
#include <math.h>
#include <iostream>
#include <iomanip>
#include "auxcalc.hh"
#include "models.hh"

// ****************************************
// for function F(_Ek, _E0, _Phi, _gamma) form models.cc
// ****************************************
// static const double r_gamma = -1./2.775;
// static const double E0 = 1.0; // GeV
// static const double gamma_ = -2.775;
// static const double Phi = 17407.0; // 1.7407 * 10^-4 (m^2 sr s GeV)^-1

// static const double r_gamma = -1./2.715;
// static const double E0 = 100.0; // GeV
// double Emin = 30.0; // GeV
// double Emax = 540.0; // GeV
// static const double gamma_ = -2.715;
// static const double Phi = 0.0466; // 4.66 * 10^-2 (m^2 sr s GeV)^-1
// ****************************************

// std::random_device rd;
// std::mt19937 e2(rd());
// std::knuth_b e2(rd());

// ****************************
double rand_t(void) {   return static_cast <double>(rand()) / RAND_MAX;   }

// ****************************
void randomize(void) {
    // global_urng().seed( time(0)) );
    srand(static_cast <unsigned> (time(0)));
}

// ****************************
double rndm_energy(double _r_gamma, double _E0, double _Emin, double _Emax, double _Phi) {
    double u = rand_t();
    double F = u;
    return _E0 * std::pow(F / _Phi, _r_gamma);
    // return _Emin + (_Emax - _Emin) * std::pow(u, _r_gamma);
}
// ****************************

// Интегрирование функции J по T в логарифмических координатах и определение среднего
// значения величины в заданном интервале; предполагается, что аргумент функции и границы
// интегрирования (Emax, Emin) измеряются в ГэВах. Суммируются значения в N точках.
void log_mean_integrate(double (*J)(double), double Emin, double Emax, int N, TIResult* r) {

    double xi, xi_prev, xi_curr;

    double ln10 = std::log(10.0);
    double xi_min = std::log(Emin) / ln10;
    double xi_max = std::log(Emax) / ln10;

    double delta_xi = xi_max - xi_min;
    double d_xi = delta_xi / static_cast<double>(N) * ln10;

    double J_xi_prev, J_xi_curr, J_xi;

    double S = 0.0;
    double Sexp = 0.0;

    // Трапеция
    xi_curr = xi_min;
    for (int i = 0; i < N; ++i) {

        xi_prev = xi_curr;
        xi_curr = xi_min + static_cast<double>(i+1) / static_cast<double>(N) * delta_xi;
        xi = xi_min + (static_cast<double>(i) + 0.5) / static_cast<double>(N) * delta_xi;

        J_xi_prev = 0.25 * J(std::exp(xi_prev * ln10));
        J_xi_curr = 0.25 * J(std::exp(xi_curr * ln10));
        J_xi = 0.5 * J(std::exp(xi * ln10));

        // Integral
        S += (

              J_xi_prev * std::exp(xi_prev * ln10)
            + J_xi_curr * std::exp(xi_curr * ln10)
            + J_xi      * std::exp(xi * ln10)

        ) * d_xi;

        // Math. exp.
        Sexp += (

              J_xi_prev * std::exp(2.0 * xi_prev * ln10)
            + J_xi_curr * std::exp(2.0 * xi_curr * ln10)
            + J_xi      * std::exp(2.0 * xi * ln10)

            ) * d_xi;

    }

    r->S = S;
    r->Mean = Sexp / S;

    return;
}
// ****************************

// Интегрирование функции J по T в линейных координатах; определение среднего
// (математического ожидания); предполагается, что аргумент функции и границы
// интегрирования (Emax, Emin) измеряются в ГэВах. Суммируются значения в N точках.
void mean_integrate(double (*J)(double), double Emin, double Emax, int N, TIResult* r) {

    double E, Eprev, Ecurr;

    double deltaE = Emax - Emin;
    double dE = deltaE / static_cast<double>(N);

    double J_Eprev, J_Ecurr, J_E;

    double S = 0.0;
    double Sexp = 0.0;

    // Трапеция
    Ecurr = Emin;
    for (int i = 0; i < N; ++i) {

        Eprev = Ecurr;
        Ecurr = Emin + static_cast<double>(i+1) / static_cast<double>(N) * deltaE;
        E = Emin + (static_cast<double>(i) + 0.5) / static_cast<double>(N) * deltaE;

        J_Eprev = 0.25 * J(Eprev);
        J_Ecurr = 0.25 * J(Ecurr);
        J_E = 0.5 * J(E);

        S += (J_Eprev + J_Ecurr + J_E) * dE;
        Sexp += (J_Eprev * Eprev + J_Ecurr * Ecurr + J_E * E) * dE;
    }

    r->S = S;
    r->Mean = Sexp / S;

    return;
}
// ****************************

// Интегрирование функции J по T в логарифмических координатах; предполагается,
// что аргумент функции и границы интегрирования (Emax, Emin) измеряются в ГэВах.
// Суммируются значения в N точках.
double logintegrate(double (*J)(double), double Emin, double Emax, int N) {

    double xi, xi_prev, xi_curr;

    double ln10 = std::log(10.0);
    double xi_min = std::log(Emin) / ln10;
    double xi_max = std::log(Emax) / ln10;

    double delta_xi = xi_max - xi_min;
    double d_xi = delta_xi / static_cast<double>(N) * ln10;

    double S = 0.0;

    // Трапеция
    xi_curr = xi_min;
    for (int i = 0; i < N; ++i) {
        xi_prev = xi_curr;
        xi_curr = xi_min + static_cast<double>(i+1) / static_cast<double>(N) * delta_xi;
        xi = xi_min + (static_cast<double>(i) + 0.5) / static_cast<double>(N) * delta_xi;
        S += (
            0.25 * (
                J(std::exp(xi_prev * ln10)) * std::exp(xi_prev * ln10) +
                J(std::exp(xi_curr * ln10)) * std::exp(xi_curr * ln10)
            )
            + 0.5 * J(std::exp(xi * ln10)) * std::exp(xi * ln10)
        ) * d_xi;
    }

    return S;
}
// ****************************

// Интегрирование функции J по T в линейных координатах; предполагается,
// что аргумент функции и границы интегрирования (Emax, Emin) измеряются в ГэВах.
// Суммируются значения в N точках.
double integrate(double (*J)(double), double Emin, double Emax, int N) {

    double E, Eprev, Ecurr;

    double deltaE = Emax - Emin;
    double dE = deltaE / static_cast<double>(N);

    double S = 0.0;

    // Трапеция
    Ecurr = Emin;
    for (int i = 0; i < N; ++i) {
        Eprev = Ecurr;
        Ecurr = Emin + static_cast<double>(i+1) / static_cast<double>(N) * deltaE;
        E = Emin + (static_cast<double>(i) + 0.5) / static_cast<double>(N) * deltaE;
        S += (0.25 * (J(Eprev) + J(Ecurr)) + 0.5 * J(E)) * dE;
    }

    return S;
}
// ****************************

// *********************************************
void gen_random_E(double r_gamma, double E0, double Emin, double Emax, double Phi) {

    int r_e = 0.0;

    for (int ccnt = 0; ccnt < 3000; ++ccnt) {

        r_e = rndm_energy(r_gamma, E0, Emin, Emax, Phi);
        // if (r_e > 300) {
        // if (r_e < 30) {
        {
            std::cout << "cnt: " << ccnt << "; "
                 << r_e << " GeV" << std::endl;
        }
    }

    return;
}

// *********************************************
void random_sum(void) {
    double sumx = 0.0;
    int count = 0;
    for (int i = 0; i < 1000; ++i) {
        // std::cout << sumx << std::endl;
        sumx += rand_t();
        count += 1;
    }

    std::cout << sumx/count << std::endl;
}

// *********************************************
void models_info(void) {
    print_J_T(J_GM75);
    print_J_T(J_BH00);
    print_J_T(J_WH03);
    print_J_T(J_LA03);
    print_J_T(J_WH09);

    std::cout << "== J_GM75: ==" << std::endl;
    std::cout << integrate(J_GM75, 1E-03, 1E+03, 32000) << std::endl;
    std::cout << logintegrate(J_GM75, 1E-03, 1E+03, 32000) << std::endl << std::endl;

    std::cout << "== J_BH00: ==" << std::endl;
    std::cout << integrate(J_BH00, 1E-03, 1E+03, 32000) << std::endl;
    std::cout << logintegrate(J_BH00, 1E-03, 1E+03, 32000) << std::endl << std::endl;

    std::cout << "== J_WH03: ==" << std::endl;
    std::cout << integrate(J_WH03, 1E-03, 1E+03, 32000) << std::endl;
    std::cout << logintegrate(J_WH03, 1E-03, 1E+03, 32000) << std::endl << std::endl;

    std::cout << "== J_LA03: ==" << std::endl;
    std::cout << integrate(J_LA03, 1E-03, 1E+03, 32000) << std::endl;
    std::cout << logintegrate(J_LA03, 1E-03, 1E+03, 32000) << std::endl << std::endl;

    std::cout << "== J_WH09: ==" << std::endl;
    std::cout << integrate(J_WH09, 1E-02, 1E+03, 32000) << std::endl;
    std::cout << logintegrate(J_WH09, 1E-02, 1E+03, 32000) << std::endl << std::endl;

    return;
}

// //////////////////////
// **********************
void print_current_time(void) {
    // std::time_t result = std::time(nullptr);
    std::time_t result = std::time(NULL);

    std::cout << "\n====================================" << std::endl;
    std::cout << std::asctime(std::localtime(&result))
              << result << " seconds since the Epoch\n";
    std::cout << "====================================\n" << std::endl;
}

// //////////////////////
// **********************
void print_J_T(double (*J)(double)) {
    double _dE = 1.0E-03;
    double dm = 1.002;
    int _cnt = 0;

    double EE = 0.0;

    std::cout
        << "E;"
        << "J_E"
        << std::endl;

    do {
        _cnt += 1;
        EE += _dE;
        _dE *= dm;

        std::cout
            << std::scientific
            << EE << ";"
            << std::setprecision(5)
            << J(EE) << ""
            << std::endl;

    } while (EE <= 1000.0);

} // print_J_T(double (*J)(double))
// //////////////////////
