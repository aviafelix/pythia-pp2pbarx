#!/usr/bin/env bash
echo " *** begin of run"
date
echo " -- go to examples/ dir"
# pushd examples/
cd examples/
echo " -- path: `pwd`"
echo " -- starting prog..."
date
# set processes number (== processor cores number)
procscount=$(nproc)
wcounter=$(nproc)
bash --version
echo " * * * * * * * * * * * * * *"
uname -r
echo " * * * * * * * * * * * * * *"
uname -a
echo " * * * * * * * * * * * * * *"
cat /proc/version
echo " * * * * * * * * * * * * * *"
cat /etc/*release
echo " * * * * * * * * * * * * * *"
echo $SHELL
echo " * * * * * * * * * * * * * *"
gcc --version
echo " * * * * * * * * * * * * * *"
whoami
hostname
echo " * * * * * * * * * * * * * *"
date
while  [ $wcounter -gt 0 ];
do
    #wcounter=$(expr $wcounter - 1)
    wcounter=$((wcounter-1))
    #let wcounter=$wcounter-1
    #let wcounter-=1
    echo "starting process #$(printf "%02d" ${wcounter})"
    ./pp2pbarx \
	--model=WH03 \
	--e-min=1.0E-03 \
	--e-max=1.0E+03 \
	--events-number=2000000 \
	--total-segments=${procscount} \
	--strips-in-segment=125 \
	--segment=${wcounter} \
	--cmnd-file=pp2pbarx.cmnd \
	--output-file-prefix=results/tmp_$(printf "%02d" ${wcounter}) > results/logs/out_$(printf "%02d" ${wcounter}).log 2>&1 &
done
# ./pp2pbarx pp2pbarx.cmnd ../result.dat > /dev/null
# 
echo " -- waiting for jobs"
# for job in `jobs -p`
# do
# 	echo $job
# 	wait $job
# done
wait
# 
echo " -- ok; end of jobs"
date
echo " -- tarring output logs:"
# 
cat results/logs/out_0.log
#cat out0 out1 out2 out3 out4 out5 out6 out7 | gzip -c - > ../out.gz
#rm out0 out1 out2 out3 out4 out5 out6 out7
pwd
ls -la
cd ..
pwd
ls -la
echo " -- cat out results:"
echo "tarring them into result.dat..."
rm result.dat
pwd
ls -la
tar -czvf WH03_pprim_2kk_sis_125_p_16.tar.gz examples/results
pwd
ls -la
rm -rf examples/results
echo " -- Ok"
date
# popd
cd ../
echo " *** end of run"
