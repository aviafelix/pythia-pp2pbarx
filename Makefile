# Makefile is a part of the PYTHIA event generator.
# Copyright (C) 2016 Torbjorn Sjostrand.
# PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.
# Author: Philip Ilten, September 2014.
#
# This is is the Makefile used to build PYTHIA examples on POSIX systems.
# Example usage is:
#     make main01
# For help using the make command please consult the local system documentation,
# i.e. "man make" or "make --help".

################################################################################
# VARIABLES: Definition of the relevant variables from the configuration script.
################################################################################

# Include the configuration.
-include Makefile.inc

# Handle GZIP support.
ifeq ($(GZIP_USE),true)
  CXX_COMMON+= -DGZIPSUPPORT -I$(GZIP_INCLUDE)
  CXX_COMMON+= -L$(GZIP_LIB) -Wl,-rpath,$(GZIP_LIB) -lz
endif

# Check distribution (use local version first, then installed version).
ifneq ("$(wildcard ../lib/libpythia8.a)","")
  PREFIX_LIB=libpythia8/lib
  PREFIX_INCLUDE=libpythia8/include
endif

LOCAL_BIN=bin
LOCAL_INCLUDE=include
LOCAL_LIB=lib
LOCAL_SRC=src
LOCAL_TMP=tmp
LOCAL_MKDIRS:=$(shell mkdir -p $(LOCAL_TMP) $(LOCAL_BIN))

CXX_COMMON:=-I$(PREFIX_INCLUDE) $(CXX_COMMON) -Wl,-rpath,$(PREFIX_LIB) -ldl -lboost_program_options
CPP_FLAGS:=-I$(PREFIX_INCLUDE) -I$(LOCAL_INCLUDE) -g -c $(CXX_COMMON)

################################################################################
# RULES: Definition of the rules used to build the PYTHIA examples.
################################################################################

# Rules without physical targets (secondary expansion for specific rules).
.SECONDEXPANSION:
.PHONY: all clean

# All targets (no default behavior).
all:
	$(MAKE) $(AM_MAKEFLAGS) pp2pbarx higc

# The Makefile configuration.
Makefile.inc:
	$(error Error: PYTHIA must be configured, please run "./configure"\
                in the top PYTHIA directory)

# PYTHIA libraries.
$(PREFIX_LIB)/libpythia8.a :
	$(error Error: PYTHIA must be built, please run "make"\
                in the top PYTHIA directory)

# Examples without external dependencies.
# main% : main%.cc $(PREFIX_LIB)/libpythia8.a
# 	$(CXX) $^ -o $@ $(CXX_COMMON)

# User-written examples for tutorials, without external dependencies.
# mymain% : mymain%.cc $(PREFIX_LIB)/libpythia8.a
# 	$(CXX) $^ -o $@ $(CXX_COMMON)

# User-written examples for tutorials, without external dependencies.

$(LOCAL_TMP)/models.o: $(LOCAL_SRC)/models.cc $(LOCAL_INCLUDE)/models.hh
	$(CXX) $(LOCAL_SRC)/models.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/auxcalc.o: $(LOCAL_SRC)/auxcalc.cc $(LOCAL_INCLUDE)/auxcalc.hh
	$(CXX) $(LOCAL_SRC)/auxcalc.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/clargs.o: $(LOCAL_SRC)/clargs.cc $(LOCAL_INCLUDE)/clargs.hh $(LOCAL_INCLUDE)/models.hh
	$(CXX) $(LOCAL_SRC)/clargs.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/config.o: $(LOCAL_SRC)/config.cc $(LOCAL_INCLUDE)/config.hh
	$(CXX) $(LOCAL_SRC)/config.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/calculations.o: $(LOCAL_SRC)/calculations.cc $(LOCAL_INCLUDE)/calculations.hh
	$(CXX) $(LOCAL_SRC)/calculations.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/pp2pbarx.o: $(LOCAL_SRC)/pp2pbarx.cc $(LOCAL_INCLUDE)/models.hh $(LOCAL_INCLUDE)/pp2pbarx.hh
	$(CXX) $(LOCAL_SRC)/pp2pbarx.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/higc.o: $(LOCAL_SRC)/higc.cc $(LOCAL_INCLUDE)/higc.hh $(LOCAL_INCLUDE)/fast-cpp-csv-parser/csv.h
	g++ -std=gnu++11 $(LOCAL_SRC)/higc.cc -Ilibpythia8/include -I$(LOCAL_INCLUDE) -g -c -Ilibpythia8/include -o $@ -O2 -pedantic -W -Wall -Wshadow -fPIC -Wl,-rpath libpythia8/lib
	# $(CXX) -std=gnu++11 $(LOCAL_SRC)/higc.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/higc_funcs.o: $(LOCAL_SRC)/higc_funcs.cc $(LOCAL_INCLUDE)/higc_funcs.hh $(LOCAL_INCLUDE)/fast-cpp-csv-parser/csv.h
	g++ -std=gnu++11 $(LOCAL_SRC)/higc_funcs.cc -Ilibpythia8/include -I$(LOCAL_INCLUDE) -g -c -Ilibpythia8/include -o $@ -O2 -pedantic -W -Wall -Wshadow -fPIC -Wl,-rpath libpythia8/lib
	# $(CXX) -std=gnu++0x $(LOCAL_SRC)/higc_funcs.cc -o $@ $(CPP_FLAGS)

$(LOCAL_TMP)/higc_clargs.o: $(LOCAL_SRC)/higc_clargs.cc $(LOCAL_INCLUDE)/higc_clargs.hh
	g++ -std=gnu++11 $(LOCAL_SRC)/higc_clargs.cc -Ilibpythia8/include -I$(LOCAL_INCLUDE) -g -c -Ilibpythia8/include -o $@ -O2 -pedantic -W -Wall -Wshadow -fPIC -Wl,-rpath libpythia8/lib
    # $(CXX) -std=gnu++0x $(LOCAL_SRC)/higc_clargs.cc -o $@ $(CPP_FLAGS)

higc: $(LOCAL_TMP)/higc.o $(LOCAL_TMP)/higc_funcs.o $(LOCAL_TMP)/higc_clargs.o $(LOCAL_TMP)/models.o $(LOCAL_TMP)/auxcalc.o
	$(CXX) $^ -o $@ $(CXX_COMMON) -lpthread
	strip $@

pp2pbarx: $(LOCAL_TMP)/pp2pbarx.o $(LOCAL_TMP)/clargs.o $(LOCAL_TMP)/config.o $(LOCAL_TMP)/models.o $(LOCAL_TMP)/auxcalc.o $(LOCAL_TMP)/calculations.o $(PREFIX_LIB)/libpythia8.a
	$(CXX) $^ -o $@ $(CXX_COMMON)
	strip $@

tests/test-clargs.o: tests/test-clargs.cc tests/test-clargs.hh models.hh
	$(CXX) tests/test-clargs.cc $(CPP_FLAGS) -o tests/test-clargs.o

test-clargs: tests/test-clargs.o clargs.o models.o
	$(CXX) $^ -o tests/$@ $(CXX_COMMON)
	strip tests/$@

test-csv-parser: tests/test-csv-parser.cc tests/test-csv-parser.hh fast-cpp-csv-parser/csv.h
	g++ -std=gnu++0x tests/test-csv-parser.cc -o tests/$@ -lpthread
	strip tests/$@

csvread:
	g++ tests/csvread.cc -o tests/$@
	strip tests/$@

tests: test-clargs test-csv-parser csvread

# Clean.
clean:
	@rm -f main[0-9][0-9]; rm -f out[0-9][0-9]; \
	rm -f pp2pbarx; rm -f higc; rm -f *.o; rm -f tests/*.o; \
	rm -f mymain[0-9][0-9]; rm -f myout[0-9][0-9]; \
	rm -f test[0-9][0-9][0-9]; rm -f out[0-9][0-9][0-9]; \
	rm -f weakbosons.lhe; rm -f Pythia8.promc; rm -f hist.root; \
	rm -f *~; rm -f \#*; rm -f core*; rm -f *Dct.*
	rm -rf $(LOCAL_TMP) $(LOCAL_BIN)
