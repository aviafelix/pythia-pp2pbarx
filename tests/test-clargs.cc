// test-clargs.cc

#include <iostream>
#include <string>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "test-clargs.hh"
#include "../clargs.hh"
#include "../models.hh"

// http://boost.cowic.de/rc/pdf/program_options.pdf
// http://www.boost.org/doc/libs/1_41_0/doc/html/program_options/tutorial.html
// http://www.boost.org/doc/libs/1_59_0/doc/html/program_options/tutorial.html
// http://pastebin.com/yBmfCgvr
// http://www.radmangames.com/programming/how-to-use-boost-program_options

int main (int argc, char* argv[]) {

    po::variables_map vm;

    try {

        vm = process_options(argc, argv);

    }
    catch (std::exception& e) {

        std::cout << "[ End of prog ]" << std::endl;

        return -1;

    }

    if (vm.count("help")) {

        return 1;

    }

    if (vm.count("model")) {

        std::cout << "model name: \""
        << vm["model"].as<std::string>()
        << "\"." << std::endl;

    } else {

        std::cout << "Model was not set" << std::endl;

        return -1;
    }

    return 0;
}