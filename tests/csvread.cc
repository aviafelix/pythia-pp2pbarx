#include <iostream>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <cstdio>
#include <vector>

// aux funcs
// ***********************
unsigned int countLines( const std::vector <char> &buff, int sz )
{
    unsigned int linescount = 0;
    const char *p = &buff[0];

    for ( int i = 0; i < sz; i++ )
    {
        if ( p[i] == '\n' )
        {
            ++linescount;
        }
    }

    return linescount;
}
// ***********************

// ***********************
unsigned int fileRead( std::istream &is, std::vector <char> &buff )
{
    is.read( &buff[0], buff.size() );

    return is.gcount();
}
// ***********************

// ***********************
struct TestEOL
{
    bool operator()(char c)
    {
        last = c;

        return last == '\n';
    }

    char last;
};
// ***********************


// #1 ~4.5s
unsigned int number_of_lines_a(char const *filename)
{
    std::string line;
    std::ifstream in( filename );
    unsigned int linescount = 0;

    while ( std::getline(in, line) )
    {
        ++linescount;
    }

    in.close();

    return linescount;
}

// #2 @ ~1m10s
unsigned int number_of_lines_b(char const *filename)
{
    std::ifstream in( filename );
    in.unsetf(std::ios_base::skipws);

    unsigned int linescount = std::count(
        std::istreambuf_iterator<char>(in),
        std::istreambuf_iterator<char>(),
        '\n');

    in.close();

    return linescount;
}

// #3 @ ~10s
unsigned int number_of_lines_c_style(char const *filename)
{
    unsigned int linescount = 0;
    FILE *fin = fopen( filename, "r" );
    unsigned int ch;

    while ( EOF != (ch = getc(fin)) )
    {
        if ('\n' == ch)
        {
            ++linescount;
        }
    }

    fclose(fin);

    return linescount;
}

// #4 @ ~1m16s
unsigned int number_of_lines_d(char const *filename)
{
    std::ifstream in( filename );
    in.unsetf(std::ios_base::skipws);
    TestEOL test;

    unsigned int linescount = std::count_if(
        std::istreambuf_iterator<char>(in),
        std::istreambuf_iterator<char>(),
        test);

    in.close();

    return linescount;
}

// #5 @ ~3.3s
unsigned int number_of_lines_e(char const *filename)
{
    const int BFSIZE = 1024 * 1024;
    std::vector <char> buff( BFSIZE );

    std::ifstream in( filename );

    unsigned int linescount = 0;

    while ( int cnt = fileRead(in, buff) )
    {
        linescount += countLines( buff, cnt );
    }

    in.close();

    return linescount;
}

int main(int argc, char const *argv[])
{
    if (argc > 1) {

        std::cout
            << " processing: "
            << argv[1]
            << std::endl;

    } else {

        std::cout
            << "   Usage: prog <csv.name>"
            << std::endl;

        return -1;
    }

    std::cout
        << " Number of lines: "
        // << number_of_lines_a(argv[1])
        // << number_of_lines_b(argv[1])
        // << number_of_lines_c_style(argv[1])
        // << number_of_lines_d(argv[1])
        << number_of_lines_e(argv[1])
        << std::endl;
    
    return 0;
}
