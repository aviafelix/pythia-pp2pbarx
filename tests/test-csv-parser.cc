// test-ccsv-parser.cc

#include <iostream>
#include <fstream>
#include <vector>
#include "../fast-cpp-csv-parser/csv.h"
#include "test-csv-parser.hh"

int main(int argc, char const *argv[])
{

    if (argc > 1) {

        std::cout
            << " processing: "
            << argv[1]
            << std::endl;

    } else {

        std::cout
            << "   Usage: prog <csv.name>"
            << std::endl;

        return -1;
    }

    io::CSVReader<
        1,
        io::trim_chars< ' ', '\t' >,
        io::no_quote_escape< ';' >
    > in( argv[1] );

    unsigned int linescount = in.get_number_of_lines();

    std::vector < double > energy_values;
    energy_values.reserve(linescount);

    in.read_header( io::ignore_extra_column, "particle_e_kin" );

    double particle_e_kin;

    while ( in.read_row(particle_e_kin) )
    {
        energy_values.push_back(particle_e_kin);
    }

    std::cout
        << " count values: "
        << energy_values.size()
        << std::endl;

    return 0;
}
